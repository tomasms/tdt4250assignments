/**
 */
package courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Assignment#getAssignmentWeight <em>Assignment Weight</em>}</li>
 *   <li>{@link courses.Assignment#getCourseInstance <em>Course Instance</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends EObject {
	/**
	 * Returns the value of the '<em><b>Assignment Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Weight</em>' attribute.
	 * @see #setAssignmentWeight(double)
	 * @see courses.CoursesPackage#getAssignment_AssignmentWeight()
	 * @model
	 * @generated
	 */
	double getAssignmentWeight();

	/**
	 * Sets the value of the '{@link courses.Assignment#getAssignmentWeight <em>Assignment Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Weight</em>' attribute.
	 * @see #getAssignmentWeight()
	 * @generated
	 */
	void setAssignmentWeight(double value);

	/**
	 * Returns the value of the '<em><b>Course Instance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getAssignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instance</em>' container reference.
	 * @see #setCourseInstance(CourseInstance)
	 * @see courses.CoursesPackage#getAssignment_CourseInstance()
	 * @see courses.CourseInstance#getAssignment
	 * @model opposite="assignment" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourseInstance();

	/**
	 * Sets the value of the '{@link courses.Assignment#getCourseInstance <em>Course Instance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Instance</em>' container reference.
	 * @see #getCourseInstance()
	 * @generated
	 */
	void setCourseInstance(CourseInstance value);

} // Assignment
