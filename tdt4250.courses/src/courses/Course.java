/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Course#getContent <em>Content</em>}</li>
 *   <li>{@link courses.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link courses.Course#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link courses.Course#getCourseInstance <em>Course Instance</em>}</li>
 *   <li>{@link courses.Course#getPrecondition <em>Precondition</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends Named, Code, Coursework {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see courses.CoursesPackage#getCourse_Content()
	 * @model required="true"
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link courses.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see courses.CoursesPackage#getCourse_Credits()
	 * @model required="true" derived="true"
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link courses.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Study Program</b></em>' reference list.
	 * The list contents are of type {@link courses.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link courses.StudyProgram#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Program</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Program</em>' reference list.
	 * @see courses.CoursesPackage#getCourse_StudyProgram()
	 * @see courses.StudyProgram#getCourse
	 * @model opposite="course"
	 * @generated
	 */
	EList<StudyProgram> getStudyProgram();

	/**
	 * Returns the value of the '<em><b>Course Instance</b></em>' containment reference list.
	 * The list contents are of type {@link courses.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instance</em>' containment reference list.
	 * @see courses.CoursesPackage#getCourse_CourseInstance()
	 * @see courses.CourseInstance#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<CourseInstance> getCourseInstance();

	/**
	 * Returns the value of the '<em><b>Precondition</b></em>' reference list.
	 * The list contents are of type {@link courses.PreconditionForCourse}.
	 * It is bidirectional and its opposite is '{@link courses.PreconditionForCourse#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precondition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precondition</em>' reference list.
	 * @see courses.CoursesPackage#getCourse_Precondition()
	 * @see courses.PreconditionForCourse#getCourse
	 * @model opposite="Course"
	 * @generated
	 */
	EList<PreconditionForCourse> getPrecondition();

} // Course
