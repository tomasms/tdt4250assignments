/**
 */
package courses;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.CourseInstance#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link courses.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link courses.CourseInstance#getCourseAffiliation <em>Course Affiliation</em>}</li>
 *   <li>{@link courses.CourseInstance#getExam <em>Exam</em>}</li>
 *   <li>{@link courses.CourseInstance#getProject <em>Project</em>}</li>
 *   <li>{@link courses.CourseInstance#getAssignment <em>Assignment</em>}</li>
 *   <li>{@link courses.CourseInstance#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='hasCoordinator evaluationSumIs100'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link courses.Timetable#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' containment reference.
	 * @see #setTimetable(Timetable)
	 * @see courses.CoursesPackage#getCourseInstance_Timetable()
	 * @see courses.Timetable#getCourse
	 * @model opposite="course" containment="true" required="true"
	 * @generated
	 */
	Timetable getTimetable();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getTimetable <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' containment reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(Timetable value);

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see courses.CoursesPackage#getCourseInstance_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Course Affiliation</b></em>' reference list.
	 * The list contents are of type {@link courses.PersonAtUniversity}.
	 * It is bidirectional and its opposite is '{@link courses.PersonAtUniversity#getCourseAffiliation <em>Course Affiliation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Affiliation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Affiliation</em>' reference list.
	 * @see courses.CoursesPackage#getCourseInstance_CourseAffiliation()
	 * @see courses.PersonAtUniversity#getCourseAffiliation
	 * @model opposite="CourseAffiliation"
	 * @generated
	 */
	EList<PersonAtUniversity> getCourseAffiliation();

	/**
	 * Returns the value of the '<em><b>Exam</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link courses.Exam#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exam</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exam</em>' containment reference.
	 * @see #setExam(Exam)
	 * @see courses.CoursesPackage#getCourseInstance_Exam()
	 * @see courses.Exam#getCourseInstance
	 * @model opposite="courseInstance" containment="true"
	 * @generated
	 */
	Exam getExam();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getExam <em>Exam</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exam</em>' containment reference.
	 * @see #getExam()
	 * @generated
	 */
	void setExam(Exam value);

	/**
	 * Returns the value of the '<em><b>Project</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link courses.Project#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' containment reference.
	 * @see #setProject(Project)
	 * @see courses.CoursesPackage#getCourseInstance_Project()
	 * @see courses.Project#getCourseInstance
	 * @model opposite="courseInstance" containment="true"
	 * @generated
	 */
	Project getProject();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getProject <em>Project</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project</em>' containment reference.
	 * @see #getProject()
	 * @generated
	 */
	void setProject(Project value);

	/**
	 * Returns the value of the '<em><b>Assignment</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link courses.Assignment#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment</em>' containment reference.
	 * @see #setAssignment(Assignment)
	 * @see courses.CoursesPackage#getCourseInstance_Assignment()
	 * @see courses.Assignment#getCourseInstance
	 * @model opposite="CourseInstance" containment="true"
	 * @generated
	 */
	Assignment getAssignment();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getAssignment <em>Assignment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment</em>' containment reference.
	 * @see #getAssignment()
	 * @generated
	 */
	void setAssignment(Assignment value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.Course#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see courses.CoursesPackage#getCourseInstance_Course()
	 * @see courses.Course#getCourseInstance
	 * @model opposite="courseInstance" required="true" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link courses.CourseInstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

} // CourseInstance
