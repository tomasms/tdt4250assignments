/**
 */
package courses;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see courses.CoursesFactory
 * @model kind="package"
 * @generated
 */
public interface CoursesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "courses";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/tdt4250.courses/model/courses.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "courses";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoursesPackage eINSTANCE = courses.impl.CoursesPackageImpl.init();

	/**
	 * The meta object id for the '{@link courses.Named <em>Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.Named
	 * @see courses.impl.CoursesPackageImpl#getNamed()
	 * @generated
	 */
	int NAMED = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.CourseImpl
	 * @see courses.impl.CoursesPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LECTURE_HOURS = NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LAB_HOURS = NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Study Program</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__STUDY_PROGRAM = NAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Course Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INSTANCE = NAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__PRECONDITION = NAMED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = NAMED_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link courses.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.StudyProgramImpl
	 * @see courses.impl.CoursesPackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Student</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__STUDENT = NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__COURSE = NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = NAMED_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link courses.Code <em>Code</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.Code
	 * @see courses.impl.CoursesPackageImpl#getCode()
	 * @generated
	 */
	int CODE = 3;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE__CODE = 0;

	/**
	 * The number of structural features of the '<em>Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.CourseworkImpl <em>Coursework</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.CourseworkImpl
	 * @see courses.impl.CoursesPackageImpl#getCoursework()
	 * @generated
	 */
	int COURSEWORK = 6;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.TimetableImpl <em>Timetable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.TimetableImpl
	 * @see courses.impl.CoursesPackageImpl#getTimetable()
	 * @generated
	 */
	int TIMETABLE = 4;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__LECTURE_HOURS = COURSEWORK__LECTURE_HOURS;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__LAB_HOURS = COURSEWORK__LAB_HOURS;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__COURSE = COURSEWORK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__ROOM = COURSEWORK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_FEATURE_COUNT = COURSEWORK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_OPERATION_COUNT = COURSEWORK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link courses.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.DepartmentImpl
	 * @see courses.impl.CoursesPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Course</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSE = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Study Program</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STUDY_PROGRAM = NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link courses.impl.PersonAtUniversityImpl <em>Person At University</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.PersonAtUniversityImpl
	 * @see courses.impl.CoursesPackageImpl#getPersonAtUniversity()
	 * @generated
	 */
	int PERSON_AT_UNIVERSITY = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>University</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__UNIVERSITY = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Course Affiliation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__COURSE_AFFILIATION = NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Total Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__TOTAL_CREDITS = NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__ROLE = NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Take Exam</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__TAKE_EXAM = NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Passed Exams</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY__PASSED_EXAMS = NAMED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Person At University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY_FEATURE_COUNT = NAMED_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Sign Up For Exam</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY___SIGN_UP_FOR_EXAM__EXAM = NAMED_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Cancel Exam</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY___CANCEL_EXAM__EXAM = NAMED_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Taking Exam</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY___TAKING_EXAM__EXAM = NAMED_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Person At University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_AT_UNIVERSITY_OPERATION_COUNT = NAMED_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link courses.impl.UniversityImpl <em>University</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.UniversityImpl
	 * @see courses.impl.CoursesPackageImpl#getUniversity()
	 * @generated
	 */
	int UNIVERSITY = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Department</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__DEPARTMENT = NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Person</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__PERSON = NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_FEATURE_COUNT = NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link courses.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.CourseInstanceImpl
	 * @see courses.impl.CoursesPackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 9;

	/**
	 * The feature id for the '<em><b>Timetable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__TIMETABLE = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 1;

	/**
	 * The feature id for the '<em><b>Course Affiliation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE_AFFILIATION = 2;

	/**
	 * The feature id for the '<em><b>Exam</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EXAM = 3;

	/**
	 * The feature id for the '<em><b>Project</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__PROJECT = 4;

	/**
	 * The feature id for the '<em><b>Assignment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__ASSIGNMENT = 5;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE = 6;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.PreconditionForCourseImpl <em>Precondition For Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.PreconditionForCourseImpl
	 * @see courses.impl.CoursesPackageImpl#getPreconditionForCourse()
	 * @generated
	 */
	int PRECONDITION_FOR_COURSE = 10;

	/**
	 * The feature id for the '<em><b>Credit Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE__CREDIT_REDUCTION = 0;

	/**
	 * The feature id for the '<em><b>Recommended Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE = 1;

	/**
	 * The feature id for the '<em><b>Required Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE__REQUIRED_COURSE = 2;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE__COURSE = 3;

	/**
	 * The feature id for the '<em><b>Similar Course</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE__SIMILAR_COURSE = 4;

	/**
	 * The number of structural features of the '<em>Precondition For Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Precondition For Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECONDITION_FOR_COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.ExamImpl <em>Exam</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.ExamImpl
	 * @see courses.impl.CoursesPackageImpl#getExam()
	 * @generated
	 */
	int EXAM = 11;

	/**
	 * The feature id for the '<em><b>Exam Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAM__EXAM_WEIGHT = 0;

	/**
	 * The feature id for the '<em><b>Person Taking Exam</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAM__PERSON_TAKING_EXAM = 1;

	/**
	 * The feature id for the '<em><b>Course Instance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAM__COURSE_INSTANCE = 2;

	/**
	 * The number of structural features of the '<em>Exam</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Exam</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.AssignmentImpl
	 * @see courses.impl.CoursesPackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 12;

	/**
	 * The feature id for the '<em><b>Assignment Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__ASSIGNMENT_WEIGHT = 0;

	/**
	 * The feature id for the '<em><b>Course Instance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__COURSE_INSTANCE = 1;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.impl.ProjectImpl <em>Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.impl.ProjectImpl
	 * @see courses.impl.CoursesPackageImpl#getProject()
	 * @generated
	 */
	int PROJECT = 13;

	/**
	 * The feature id for the '<em><b>Project Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__PROJECT_WEIGHT = 0;

	/**
	 * The feature id for the '<em><b>Course Instance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__COURSE_INSTANCE = 1;

	/**
	 * The number of structural features of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link courses.Role <em>Role</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see courses.Role
	 * @see courses.impl.CoursesPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 14;

	/**
	 * Returns the meta object for class '{@link courses.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see courses.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link courses.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see courses.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the attribute '{@link courses.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see courses.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the reference list '{@link courses.Course#getStudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Study Program</em>'.
	 * @see courses.Course#getStudyProgram()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_StudyProgram();

	/**
	 * Returns the meta object for the containment reference list '{@link courses.Course#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Instance</em>'.
	 * @see courses.Course#getCourseInstance()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInstance();

	/**
	 * Returns the meta object for the reference list '{@link courses.Course#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Precondition</em>'.
	 * @see courses.Course#getPrecondition()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Precondition();

	/**
	 * Returns the meta object for class '{@link courses.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see courses.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the reference list '{@link courses.StudyProgram#getStudent <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Student</em>'.
	 * @see courses.StudyProgram#getStudent()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Student();

	/**
	 * Returns the meta object for the reference list '{@link courses.StudyProgram#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Course</em>'.
	 * @see courses.StudyProgram#getCourse()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Course();

	/**
	 * Returns the meta object for class '{@link courses.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named</em>'.
	 * @see courses.Named
	 * @generated
	 */
	EClass getNamed();

	/**
	 * Returns the meta object for the attribute '{@link courses.Named#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see courses.Named#getName()
	 * @see #getNamed()
	 * @generated
	 */
	EAttribute getNamed_Name();

	/**
	 * Returns the meta object for class '{@link courses.Code <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code</em>'.
	 * @see courses.Code
	 * @generated
	 */
	EClass getCode();

	/**
	 * Returns the meta object for the attribute '{@link courses.Code#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see courses.Code#getCode()
	 * @see #getCode()
	 * @generated
	 */
	EAttribute getCode_Code();

	/**
	 * Returns the meta object for class '{@link courses.Timetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timetable</em>'.
	 * @see courses.Timetable
	 * @generated
	 */
	EClass getTimetable();

	/**
	 * Returns the meta object for the container reference '{@link courses.Timetable#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see courses.Timetable#getCourse()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_Course();

	/**
	 * Returns the meta object for the attribute list '{@link courses.Timetable#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Room</em>'.
	 * @see courses.Timetable#getRoom()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_Room();

	/**
	 * Returns the meta object for class '{@link courses.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see courses.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the containment reference list '{@link courses.Department#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course</em>'.
	 * @see courses.Department#getCourse()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Course();

	/**
	 * Returns the meta object for the containment reference list '{@link courses.Department#getStudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Study Program</em>'.
	 * @see courses.Department#getStudyProgram()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_StudyProgram();

	/**
	 * Returns the meta object for class '{@link courses.Coursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coursework</em>'.
	 * @see courses.Coursework
	 * @generated
	 */
	EClass getCoursework();

	/**
	 * Returns the meta object for the attribute list '{@link courses.Coursework#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lecture Hours</em>'.
	 * @see courses.Coursework#getLectureHours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_LectureHours();

	/**
	 * Returns the meta object for the attribute list '{@link courses.Coursework#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lab Hours</em>'.
	 * @see courses.Coursework#getLabHours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_LabHours();

	/**
	 * Returns the meta object for class '{@link courses.PreconditionForCourse <em>Precondition For Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Precondition For Course</em>'.
	 * @see courses.PreconditionForCourse
	 * @generated
	 */
	EClass getPreconditionForCourse();

	/**
	 * Returns the meta object for the attribute '{@link courses.PreconditionForCourse#getCreditReduction <em>Credit Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credit Reduction</em>'.
	 * @see courses.PreconditionForCourse#getCreditReduction()
	 * @see #getPreconditionForCourse()
	 * @generated
	 */
	EAttribute getPreconditionForCourse_CreditReduction();

	/**
	 * Returns the meta object for the attribute '{@link courses.PreconditionForCourse#isRecommendedCourse <em>Recommended Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Recommended Course</em>'.
	 * @see courses.PreconditionForCourse#isRecommendedCourse()
	 * @see #getPreconditionForCourse()
	 * @generated
	 */
	EAttribute getPreconditionForCourse_RecommendedCourse();

	/**
	 * Returns the meta object for the attribute '{@link courses.PreconditionForCourse#isRequiredCourse <em>Required Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required Course</em>'.
	 * @see courses.PreconditionForCourse#isRequiredCourse()
	 * @see #getPreconditionForCourse()
	 * @generated
	 */
	EAttribute getPreconditionForCourse_RequiredCourse();

	/**
	 * Returns the meta object for the reference list '{@link courses.PreconditionForCourse#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Course</em>'.
	 * @see courses.PreconditionForCourse#getCourse()
	 * @see #getPreconditionForCourse()
	 * @generated
	 */
	EReference getPreconditionForCourse_Course();

	/**
	 * Returns the meta object for the attribute list '{@link courses.PreconditionForCourse#getSimilarCourse <em>Similar Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Similar Course</em>'.
	 * @see courses.PreconditionForCourse#getSimilarCourse()
	 * @see #getPreconditionForCourse()
	 * @generated
	 */
	EAttribute getPreconditionForCourse_SimilarCourse();

	/**
	 * Returns the meta object for class '{@link courses.Exam <em>Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exam</em>'.
	 * @see courses.Exam
	 * @generated
	 */
	EClass getExam();

	/**
	 * Returns the meta object for the attribute '{@link courses.Exam#getExamWeight <em>Exam Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exam Weight</em>'.
	 * @see courses.Exam#getExamWeight()
	 * @see #getExam()
	 * @generated
	 */
	EAttribute getExam_ExamWeight();

	/**
	 * Returns the meta object for the reference list '{@link courses.Exam#getPersonTakingExam <em>Person Taking Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Person Taking Exam</em>'.
	 * @see courses.Exam#getPersonTakingExam()
	 * @see #getExam()
	 * @generated
	 */
	EReference getExam_PersonTakingExam();

	/**
	 * Returns the meta object for the container reference '{@link courses.Exam#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course Instance</em>'.
	 * @see courses.Exam#getCourseInstance()
	 * @see #getExam()
	 * @generated
	 */
	EReference getExam_CourseInstance();

	/**
	 * Returns the meta object for class '{@link courses.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see courses.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for the attribute '{@link courses.Assignment#getAssignmentWeight <em>Assignment Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assignment Weight</em>'.
	 * @see courses.Assignment#getAssignmentWeight()
	 * @see #getAssignment()
	 * @generated
	 */
	EAttribute getAssignment_AssignmentWeight();

	/**
	 * Returns the meta object for the container reference '{@link courses.Assignment#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course Instance</em>'.
	 * @see courses.Assignment#getCourseInstance()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_CourseInstance();

	/**
	 * Returns the meta object for class '{@link courses.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project</em>'.
	 * @see courses.Project
	 * @generated
	 */
	EClass getProject();

	/**
	 * Returns the meta object for the attribute '{@link courses.Project#getProjectWeight <em>Project Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Weight</em>'.
	 * @see courses.Project#getProjectWeight()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_ProjectWeight();

	/**
	 * Returns the meta object for the container reference '{@link courses.Project#getCourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course Instance</em>'.
	 * @see courses.Project#getCourseInstance()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_CourseInstance();

	/**
	 * Returns the meta object for class '{@link courses.PersonAtUniversity <em>Person At University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person At University</em>'.
	 * @see courses.PersonAtUniversity
	 * @generated
	 */
	EClass getPersonAtUniversity();

	/**
	 * Returns the meta object for the container reference '{@link courses.PersonAtUniversity#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>University</em>'.
	 * @see courses.PersonAtUniversity#getUniversity()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EReference getPersonAtUniversity_University();

	/**
	 * Returns the meta object for the attribute '{@link courses.PersonAtUniversity#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see courses.PersonAtUniversity#getRole()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EAttribute getPersonAtUniversity_Role();

	/**
	 * Returns the meta object for the reference list '{@link courses.PersonAtUniversity#getTakeExam <em>Take Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Take Exam</em>'.
	 * @see courses.PersonAtUniversity#getTakeExam()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EReference getPersonAtUniversity_TakeExam();

	/**
	 * Returns the meta object for the reference list '{@link courses.PersonAtUniversity#getPassedExams <em>Passed Exams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Passed Exams</em>'.
	 * @see courses.PersonAtUniversity#getPassedExams()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EReference getPersonAtUniversity_PassedExams();

	/**
	 * Returns the meta object for the '{@link courses.PersonAtUniversity#signUpForExam(courses.Exam) <em>Sign Up For Exam</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Sign Up For Exam</em>' operation.
	 * @see courses.PersonAtUniversity#signUpForExam(courses.Exam)
	 * @generated
	 */
	EOperation getPersonAtUniversity__SignUpForExam__Exam();

	/**
	 * Returns the meta object for the '{@link courses.PersonAtUniversity#cancelExam(courses.Exam) <em>Cancel Exam</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Exam</em>' operation.
	 * @see courses.PersonAtUniversity#cancelExam(courses.Exam)
	 * @generated
	 */
	EOperation getPersonAtUniversity__CancelExam__Exam();

	/**
	 * Returns the meta object for the '{@link courses.PersonAtUniversity#takingExam(courses.Exam) <em>Taking Exam</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Taking Exam</em>' operation.
	 * @see courses.PersonAtUniversity#takingExam(courses.Exam)
	 * @generated
	 */
	EOperation getPersonAtUniversity__TakingExam__Exam();

	/**
	 * Returns the meta object for the reference list '{@link courses.PersonAtUniversity#getCourseAffiliation <em>Course Affiliation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Course Affiliation</em>'.
	 * @see courses.PersonAtUniversity#getCourseAffiliation()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EReference getPersonAtUniversity_CourseAffiliation();

	/**
	 * Returns the meta object for the attribute '{@link courses.PersonAtUniversity#getTotalCredits <em>Total Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Credits</em>'.
	 * @see courses.PersonAtUniversity#getTotalCredits()
	 * @see #getPersonAtUniversity()
	 * @generated
	 */
	EAttribute getPersonAtUniversity_TotalCredits();

	/**
	 * Returns the meta object for class '{@link courses.University <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>University</em>'.
	 * @see courses.University
	 * @generated
	 */
	EClass getUniversity();

	/**
	 * Returns the meta object for the containment reference list '{@link courses.University#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Department</em>'.
	 * @see courses.University#getDepartment()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Department();

	/**
	 * Returns the meta object for the containment reference list '{@link courses.University#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Person</em>'.
	 * @see courses.University#getPerson()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Person();

	/**
	 * Returns the meta object for class '{@link courses.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see courses.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the containment reference '{@link courses.CourseInstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timetable</em>'.
	 * @see courses.CourseInstance#getTimetable()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Timetable();

	/**
	 * Returns the meta object for the attribute '{@link courses.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see courses.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the reference list '{@link courses.CourseInstance#getCourseAffiliation <em>Course Affiliation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Course Affiliation</em>'.
	 * @see courses.CourseInstance#getCourseAffiliation()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_CourseAffiliation();

	/**
	 * Returns the meta object for the containment reference '{@link courses.CourseInstance#getExam <em>Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Exam</em>'.
	 * @see courses.CourseInstance#getExam()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Exam();

	/**
	 * Returns the meta object for the containment reference '{@link courses.CourseInstance#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Project</em>'.
	 * @see courses.CourseInstance#getProject()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Project();

	/**
	 * Returns the meta object for the containment reference '{@link courses.CourseInstance#getAssignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assignment</em>'.
	 * @see courses.CourseInstance#getAssignment()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Assignment();

	/**
	 * Returns the meta object for the container reference '{@link courses.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see courses.CourseInstance#getCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Course();

	/**
	 * Returns the meta object for enum '{@link courses.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Role</em>'.
	 * @see courses.Role
	 * @generated
	 */
	EEnum getRole();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoursesFactory getCoursesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link courses.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.CourseImpl
		 * @see courses.impl.CoursesPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Study Program</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__STUDY_PROGRAM = eINSTANCE.getCourse_StudyProgram();

		/**
		 * The meta object literal for the '<em><b>Course Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INSTANCE = eINSTANCE.getCourse_CourseInstance();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__PRECONDITION = eINSTANCE.getCourse_Precondition();

		/**
		 * The meta object literal for the '{@link courses.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.StudyProgramImpl
		 * @see courses.impl.CoursesPackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Student</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__STUDENT = eINSTANCE.getStudyProgram_Student();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__COURSE = eINSTANCE.getStudyProgram_Course();

		/**
		 * The meta object literal for the '{@link courses.Named <em>Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.Named
		 * @see courses.impl.CoursesPackageImpl#getNamed()
		 * @generated
		 */
		EClass NAMED = eINSTANCE.getNamed();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED__NAME = eINSTANCE.getNamed_Name();

		/**
		 * The meta object literal for the '{@link courses.Code <em>Code</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.Code
		 * @see courses.impl.CoursesPackageImpl#getCode()
		 * @generated
		 */
		EClass CODE = eINSTANCE.getCode();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE__CODE = eINSTANCE.getCode_Code();

		/**
		 * The meta object literal for the '{@link courses.impl.TimetableImpl <em>Timetable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.TimetableImpl
		 * @see courses.impl.CoursesPackageImpl#getTimetable()
		 * @generated
		 */
		EClass TIMETABLE = eINSTANCE.getTimetable();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__COURSE = eINSTANCE.getTimetable_Course();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__ROOM = eINSTANCE.getTimetable_Room();

		/**
		 * The meta object literal for the '{@link courses.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.DepartmentImpl
		 * @see courses.impl.CoursesPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSE = eINSTANCE.getDepartment_Course();

		/**
		 * The meta object literal for the '<em><b>Study Program</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STUDY_PROGRAM = eINSTANCE.getDepartment_StudyProgram();

		/**
		 * The meta object literal for the '{@link courses.impl.CourseworkImpl <em>Coursework</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.CourseworkImpl
		 * @see courses.impl.CoursesPackageImpl#getCoursework()
		 * @generated
		 */
		EClass COURSEWORK = eINSTANCE.getCoursework();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LECTURE_HOURS = eINSTANCE.getCoursework_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LAB_HOURS = eINSTANCE.getCoursework_LabHours();

		/**
		 * The meta object literal for the '{@link courses.impl.PreconditionForCourseImpl <em>Precondition For Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.PreconditionForCourseImpl
		 * @see courses.impl.CoursesPackageImpl#getPreconditionForCourse()
		 * @generated
		 */
		EClass PRECONDITION_FOR_COURSE = eINSTANCE.getPreconditionForCourse();

		/**
		 * The meta object literal for the '<em><b>Credit Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRECONDITION_FOR_COURSE__CREDIT_REDUCTION = eINSTANCE.getPreconditionForCourse_CreditReduction();

		/**
		 * The meta object literal for the '<em><b>Recommended Course</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE = eINSTANCE.getPreconditionForCourse_RecommendedCourse();

		/**
		 * The meta object literal for the '<em><b>Required Course</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRECONDITION_FOR_COURSE__REQUIRED_COURSE = eINSTANCE.getPreconditionForCourse_RequiredCourse();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRECONDITION_FOR_COURSE__COURSE = eINSTANCE.getPreconditionForCourse_Course();

		/**
		 * The meta object literal for the '<em><b>Similar Course</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRECONDITION_FOR_COURSE__SIMILAR_COURSE = eINSTANCE.getPreconditionForCourse_SimilarCourse();

		/**
		 * The meta object literal for the '{@link courses.impl.ExamImpl <em>Exam</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.ExamImpl
		 * @see courses.impl.CoursesPackageImpl#getExam()
		 * @generated
		 */
		EClass EXAM = eINSTANCE.getExam();

		/**
		 * The meta object literal for the '<em><b>Exam Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAM__EXAM_WEIGHT = eINSTANCE.getExam_ExamWeight();

		/**
		 * The meta object literal for the '<em><b>Person Taking Exam</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXAM__PERSON_TAKING_EXAM = eINSTANCE.getExam_PersonTakingExam();

		/**
		 * The meta object literal for the '<em><b>Course Instance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXAM__COURSE_INSTANCE = eINSTANCE.getExam_CourseInstance();

		/**
		 * The meta object literal for the '{@link courses.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.AssignmentImpl
		 * @see courses.impl.CoursesPackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '<em><b>Assignment Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSIGNMENT__ASSIGNMENT_WEIGHT = eINSTANCE.getAssignment_AssignmentWeight();

		/**
		 * The meta object literal for the '<em><b>Course Instance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__COURSE_INSTANCE = eINSTANCE.getAssignment_CourseInstance();

		/**
		 * The meta object literal for the '{@link courses.impl.ProjectImpl <em>Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.ProjectImpl
		 * @see courses.impl.CoursesPackageImpl#getProject()
		 * @generated
		 */
		EClass PROJECT = eINSTANCE.getProject();

		/**
		 * The meta object literal for the '<em><b>Project Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__PROJECT_WEIGHT = eINSTANCE.getProject_ProjectWeight();

		/**
		 * The meta object literal for the '<em><b>Course Instance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROJECT__COURSE_INSTANCE = eINSTANCE.getProject_CourseInstance();

		/**
		 * The meta object literal for the '{@link courses.impl.PersonAtUniversityImpl <em>Person At University</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.PersonAtUniversityImpl
		 * @see courses.impl.CoursesPackageImpl#getPersonAtUniversity()
		 * @generated
		 */
		EClass PERSON_AT_UNIVERSITY = eINSTANCE.getPersonAtUniversity();

		/**
		 * The meta object literal for the '<em><b>University</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_AT_UNIVERSITY__UNIVERSITY = eINSTANCE.getPersonAtUniversity_University();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_AT_UNIVERSITY__ROLE = eINSTANCE.getPersonAtUniversity_Role();

		/**
		 * The meta object literal for the '<em><b>Take Exam</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_AT_UNIVERSITY__TAKE_EXAM = eINSTANCE.getPersonAtUniversity_TakeExam();

		/**
		 * The meta object literal for the '<em><b>Passed Exams</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_AT_UNIVERSITY__PASSED_EXAMS = eINSTANCE.getPersonAtUniversity_PassedExams();

		/**
		 * The meta object literal for the '<em><b>Sign Up For Exam</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSON_AT_UNIVERSITY___SIGN_UP_FOR_EXAM__EXAM = eINSTANCE.getPersonAtUniversity__SignUpForExam__Exam();

		/**
		 * The meta object literal for the '<em><b>Cancel Exam</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSON_AT_UNIVERSITY___CANCEL_EXAM__EXAM = eINSTANCE.getPersonAtUniversity__CancelExam__Exam();

		/**
		 * The meta object literal for the '<em><b>Taking Exam</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERSON_AT_UNIVERSITY___TAKING_EXAM__EXAM = eINSTANCE.getPersonAtUniversity__TakingExam__Exam();

		/**
		 * The meta object literal for the '<em><b>Course Affiliation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_AT_UNIVERSITY__COURSE_AFFILIATION = eINSTANCE.getPersonAtUniversity_CourseAffiliation();

		/**
		 * The meta object literal for the '<em><b>Total Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_AT_UNIVERSITY__TOTAL_CREDITS = eINSTANCE.getPersonAtUniversity_TotalCredits();

		/**
		 * The meta object literal for the '{@link courses.impl.UniversityImpl <em>University</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.UniversityImpl
		 * @see courses.impl.CoursesPackageImpl#getUniversity()
		 * @generated
		 */
		EClass UNIVERSITY = eINSTANCE.getUniversity();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__DEPARTMENT = eINSTANCE.getUniversity_Department();

		/**
		 * The meta object literal for the '<em><b>Person</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__PERSON = eINSTANCE.getUniversity_Person();

		/**
		 * The meta object literal for the '{@link courses.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.impl.CourseInstanceImpl
		 * @see courses.impl.CoursesPackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Timetable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__TIMETABLE = eINSTANCE.getCourseInstance_Timetable();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Course Affiliation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE_AFFILIATION = eINSTANCE.getCourseInstance_CourseAffiliation();

		/**
		 * The meta object literal for the '<em><b>Exam</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EXAM = eINSTANCE.getCourseInstance_Exam();

		/**
		 * The meta object literal for the '<em><b>Project</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__PROJECT = eINSTANCE.getCourseInstance_Project();

		/**
		 * The meta object literal for the '<em><b>Assignment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__ASSIGNMENT = eINSTANCE.getCourseInstance_Assignment();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE = eINSTANCE.getCourseInstance_Course();

		/**
		 * The meta object literal for the '{@link courses.Role <em>Role</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see courses.Role
		 * @see courses.impl.CoursesPackageImpl#getRole()
		 * @generated
		 */
		EEnum ROLE = eINSTANCE.getRole();

	}

} //CoursesPackage
