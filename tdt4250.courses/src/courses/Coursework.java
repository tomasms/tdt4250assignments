/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Coursework#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link courses.Coursework#getLabHours <em>Lab Hours</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getCoursework()
 * @model abstract="true"
 * @generated
 */
public interface Coursework extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' attribute list.
	 * @see courses.CoursesPackage#getCoursework_LectureHours()
	 * @model
	 * @generated
	 */
	EList<Integer> getLectureHours();

	/**
	 * Returns the value of the '<em><b>Lab Hours</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab Hours</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab Hours</em>' attribute list.
	 * @see courses.CoursesPackage#getCoursework_LabHours()
	 * @model
	 * @generated
	 */
	EList<Integer> getLabHours();

} // Coursework
