/**
 */
package courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Credit Reduction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.CreditReduction#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getCreditReduction()
 * @model
 * @generated
 */
public interface CreditReduction extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link courses.Course#getCreditReduction <em>Credit Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference.
	 * @see #setCourse(Course)
	 * @see courses.CoursesPackage#getCreditReduction_Course()
	 * @see courses.Course#getCreditReduction
	 * @model opposite="creditReduction" derived="true"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link courses.CreditReduction#getCourse <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

} // CreditReduction
