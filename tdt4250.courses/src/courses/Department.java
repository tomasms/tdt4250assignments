/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Department#getCourse <em>Course</em>}</li>
 *   <li>{@link courses.Department#getStudyProgram <em>Study Program</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends Named {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' containment reference list.
	 * The list contents are of type {@link courses.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' containment reference list.
	 * @see courses.CoursesPackage#getDepartment_Course()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourse();

	/**
	 * Returns the value of the '<em><b>Study Program</b></em>' containment reference list.
	 * The list contents are of type {@link courses.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Program</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Program</em>' containment reference list.
	 * @see courses.CoursesPackage#getDepartment_StudyProgram()
	 * @model containment="true"
	 * @generated
	 */
	EList<StudyProgram> getStudyProgram();

} // Department
