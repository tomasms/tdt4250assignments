/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Evaluation#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link courses.Evaluation#getExamWeighting <em>Exam Weighting</em>}</li>
 *   <li>{@link courses.Evaluation#getProjectWeighting <em>Project Weighting</em>}</li>
 *   <li>{@link courses.Evaluation#getAssignmentWeighting <em>Assignment Weighting</em>}</li>
 *   <li>{@link courses.Evaluation#isAssignmentRequiredForTakingExam <em>Assignment Required For Taking Exam</em>}</li>
 *   <li>{@link courses.Evaluation#getEvaluateGrade <em>Evaluate Grade</em>}</li>
 *   <li>{@link courses.Evaluation#getStudentTakingExam <em>Student Taking Exam</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getEvaluation()
 * @model
 * @generated
 */
public interface Evaluation extends EObject {
	/**
	 * Returns the value of the '<em><b>Evaluationform</b></em>' attribute list.
	 * The list contents are of type {@link courses.Evaluationform}.
	 * The literals are from the enumeration {@link courses.Evaluationform}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationform</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationform</em>' attribute list.
	 * @see courses.Evaluationform
	 * @see courses.CoursesPackage#getEvaluation_Evaluationform()
	 * @model required="true"
	 * @generated
	 */
	EList<Evaluationform> getEvaluationform();

	/**
	 * Returns the value of the '<em><b>Exam Weighting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exam Weighting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exam Weighting</em>' attribute.
	 * @see #setExamWeighting(int)
	 * @see courses.CoursesPackage#getEvaluation_ExamWeighting()
	 * @model
	 * @generated
	 */
	int getExamWeighting();

	/**
	 * Sets the value of the '{@link courses.Evaluation#getExamWeighting <em>Exam Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exam Weighting</em>' attribute.
	 * @see #getExamWeighting()
	 * @generated
	 */
	void setExamWeighting(int value);

	/**
	 * Returns the value of the '<em><b>Project Weighting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Weighting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Weighting</em>' attribute.
	 * @see #setProjectWeighting(int)
	 * @see courses.CoursesPackage#getEvaluation_ProjectWeighting()
	 * @model
	 * @generated
	 */
	int getProjectWeighting();

	/**
	 * Sets the value of the '{@link courses.Evaluation#getProjectWeighting <em>Project Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Weighting</em>' attribute.
	 * @see #getProjectWeighting()
	 * @generated
	 */
	void setProjectWeighting(int value);

	/**
	 * Returns the value of the '<em><b>Assignment Weighting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Weighting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Weighting</em>' attribute.
	 * @see #setAssignmentWeighting(int)
	 * @see courses.CoursesPackage#getEvaluation_AssignmentWeighting()
	 * @model
	 * @generated
	 */
	int getAssignmentWeighting();

	/**
	 * Sets the value of the '{@link courses.Evaluation#getAssignmentWeighting <em>Assignment Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Weighting</em>' attribute.
	 * @see #getAssignmentWeighting()
	 * @generated
	 */
	void setAssignmentWeighting(int value);

	/**
	 * Returns the value of the '<em><b>Assignment Required For Taking Exam</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Required For Taking Exam</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Required For Taking Exam</em>' attribute.
	 * @see #setAssignmentRequiredForTakingExam(boolean)
	 * @see courses.CoursesPackage#getEvaluation_AssignmentRequiredForTakingExam()
	 * @model
	 * @generated
	 */
	boolean isAssignmentRequiredForTakingExam();

	/**
	 * Sets the value of the '{@link courses.Evaluation#isAssignmentRequiredForTakingExam <em>Assignment Required For Taking Exam</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Required For Taking Exam</em>' attribute.
	 * @see #isAssignmentRequiredForTakingExam()
	 * @generated
	 */
	void setAssignmentRequiredForTakingExam(boolean value);

	/**
	 * Returns the value of the '<em><b>Evaluate Grade</b></em>' containment reference list.
	 * The list contents are of type {@link courses.Grading}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluate Grade</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluate Grade</em>' containment reference list.
	 * @see courses.CoursesPackage#getEvaluation_EvaluateGrade()
	 * @model containment="true"
	 * @generated
	 */
	EList<Grading> getEvaluateGrade();

	/**
	 * Returns the value of the '<em><b>Student Taking Exam</b></em>' reference list.
	 * The list contents are of type {@link courses.PersonAtUniversity}.
	 * It is bidirectional and its opposite is '{@link courses.PersonAtUniversity#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Student Taking Exam</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student Taking Exam</em>' reference list.
	 * @see courses.CoursesPackage#getEvaluation_StudentTakingExam()
	 * @see courses.PersonAtUniversity#getEvaluation
	 * @model opposite="evaluation"
	 * @generated
	 */
	EList<PersonAtUniversity> getStudentTakingExam();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @model
	 * @generated NOT
	 */
	boolean checkIfSumIs100(Evaluation evaluation);

} // Evaluation
