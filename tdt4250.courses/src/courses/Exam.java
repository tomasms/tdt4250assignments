/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exam</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Exam#getExamWeight <em>Exam Weight</em>}</li>
 *   <li>{@link courses.Exam#getPersonTakingExam <em>Person Taking Exam</em>}</li>
 *   <li>{@link courses.Exam#getCourseInstance <em>Course Instance</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getExam()
 * @model
 * @generated
 */
public interface Exam extends EObject {
	/**
	 * Returns the value of the '<em><b>Exam Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exam Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exam Weight</em>' attribute.
	 * @see #setExamWeight(double)
	 * @see courses.CoursesPackage#getExam_ExamWeight()
	 * @model required="true"
	 * @generated
	 */
	double getExamWeight();

	/**
	 * Sets the value of the '{@link courses.Exam#getExamWeight <em>Exam Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exam Weight</em>' attribute.
	 * @see #getExamWeight()
	 * @generated
	 */
	void setExamWeight(double value);

	/**
	 * Returns the value of the '<em><b>Person Taking Exam</b></em>' reference list.
	 * The list contents are of type {@link courses.PersonAtUniversity}.
	 * It is bidirectional and its opposite is '{@link courses.PersonAtUniversity#getTakeExam <em>Take Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Taking Exam</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Taking Exam</em>' reference list.
	 * @see courses.CoursesPackage#getExam_PersonTakingExam()
	 * @see courses.PersonAtUniversity#getTakeExam
	 * @model opposite="takeExam"
	 * @generated
	 */
	EList<PersonAtUniversity> getPersonTakingExam();

	/**
	 * Returns the value of the '<em><b>Course Instance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getExam <em>Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instance</em>' container reference.
	 * @see #setCourseInstance(CourseInstance)
	 * @see courses.CoursesPackage#getExam_CourseInstance()
	 * @see courses.CourseInstance#getExam
	 * @model opposite="exam" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourseInstance();

	/**
	 * Sets the value of the '{@link courses.Exam#getCourseInstance <em>Course Instance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Instance</em>' container reference.
	 * @see #getCourseInstance()
	 * @generated
	 */
	void setCourseInstance(CourseInstance value);

} // Exam
