/**
 */
package courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grading</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Grading#getPersonGraded <em>Person Graded</em>}</li>
 *   <li>{@link courses.Grading#getGrade <em>Grade</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getGrading()
 * @model
 * @generated
 */
public interface Grading extends EObject {
	/**
	 * Returns the value of the '<em><b>Person Graded</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.PersonAtUniversity#getGrade <em>Grade</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Graded</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Graded</em>' container reference.
	 * @see #setPersonGraded(PersonAtUniversity)
	 * @see courses.CoursesPackage#getGrading_PersonGraded()
	 * @see courses.PersonAtUniversity#getGrade
	 * @model opposite="Grade" transient="false"
	 * @generated
	 */
	PersonAtUniversity getPersonGraded();

	/**
	 * Sets the value of the '{@link courses.Grading#getPersonGraded <em>Person Graded</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person Graded</em>' container reference.
	 * @see #getPersonGraded()
	 * @generated
	 */
	void setPersonGraded(PersonAtUniversity value);

	/**
	 * Returns the value of the '<em><b>Grade</b></em>' attribute.
	 * The literals are from the enumeration {@link courses.Grade}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grade</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grade</em>' attribute.
	 * @see courses.Grade
	 * @see #setGrade(Grade)
	 * @see courses.CoursesPackage#getGrading_Grade()
	 * @model required="true"
	 * @generated
	 */
	Grade getGrade();

	/**
	 * Sets the value of the '{@link courses.Grading#getGrade <em>Grade</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grade</em>' attribute.
	 * @see courses.Grade
	 * @see #getGrade()
	 * @generated
	 */
	void setGrade(Grade value);

} // Grading
