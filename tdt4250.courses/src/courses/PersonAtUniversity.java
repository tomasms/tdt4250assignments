/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person At University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.PersonAtUniversity#getUniversity <em>University</em>}</li>
 *   <li>{@link courses.PersonAtUniversity#getCourseAffiliation <em>Course Affiliation</em>}</li>
 *   <li>{@link courses.PersonAtUniversity#getTotalCredits <em>Total Credits</em>}</li>
 *   <li>{@link courses.PersonAtUniversity#getRole <em>Role</em>}</li>
 *   <li>{@link courses.PersonAtUniversity#getTakeExam <em>Take Exam</em>}</li>
 *   <li>{@link courses.PersonAtUniversity#getPassedExams <em>Passed Exams</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getPersonAtUniversity()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='checkIfStudent'"
 * @generated
 */
public interface PersonAtUniversity extends Named {
	/**
	 * Returns the value of the '<em><b>University</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.University#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>University</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>University</em>' container reference.
	 * @see #setUniversity(University)
	 * @see courses.CoursesPackage#getPersonAtUniversity_University()
	 * @see courses.University#getPerson
	 * @model opposite="Person" required="true" transient="false"
	 * @generated
	 */
	University getUniversity();

	/**
	 * Sets the value of the '{@link courses.PersonAtUniversity#getUniversity <em>University</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>University</em>' container reference.
	 * @see #getUniversity()
	 * @generated
	 */
	void setUniversity(University value);

	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The literals are from the enumeration {@link courses.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see courses.Role
	 * @see #setRole(Role)
	 * @see courses.CoursesPackage#getPersonAtUniversity_Role()
	 * @model required="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link courses.PersonAtUniversity#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see courses.Role
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>Take Exam</b></em>' reference list.
	 * The list contents are of type {@link courses.Exam}.
	 * It is bidirectional and its opposite is '{@link courses.Exam#getPersonTakingExam <em>Person Taking Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Take Exam</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Take Exam</em>' reference list.
	 * @see courses.CoursesPackage#getPersonAtUniversity_TakeExam()
	 * @see courses.Exam#getPersonTakingExam
	 * @model opposite="personTakingExam"
	 * @generated
	 */
	EList<Exam> getTakeExam();

	/**
	 * Returns the value of the '<em><b>Passed Exams</b></em>' reference list.
	 * The list contents are of type {@link courses.Exam}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Passed Exams</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Passed Exams</em>' reference list.
	 * @see courses.CoursesPackage#getPersonAtUniversity_PassedExams()
	 * @model
	 * @generated
	 */
	EList<Exam> getPassedExams();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model examRequired="true"
	 * @generated
	 */
	void signUpForExam(Exam exam);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model examRequired="true"
	 * @generated
	 */
	void cancelExam(Exam exam);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model examRequired="true"
	 * @generated
	 */
	void takingExam(Exam exam);

	/**
	 * Returns the value of the '<em><b>Course Affiliation</b></em>' reference list.
	 * The list contents are of type {@link courses.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getCourseAffiliation <em>Course Affiliation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Affiliation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Affiliation</em>' reference list.
	 * @see courses.CoursesPackage#getPersonAtUniversity_CourseAffiliation()
	 * @see courses.CourseInstance#getCourseAffiliation
	 * @model opposite="CourseAffiliation"
	 * @generated
	 */
	EList<CourseInstance> getCourseAffiliation();

	/**
	 * Returns the value of the '<em><b>Total Credits</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Credits</em>' attribute.
	 * @see courses.CoursesPackage#getPersonAtUniversity_TotalCredits()
	 * @model default="0.0" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	double getTotalCredits();
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @model
	 * @generated NOT
	 */

} // PersonAtUniversity
