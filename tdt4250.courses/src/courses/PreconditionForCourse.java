/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Precondition For Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.PreconditionForCourse#getCreditReduction <em>Credit Reduction</em>}</li>
 *   <li>{@link courses.PreconditionForCourse#isRecommendedCourse <em>Recommended Course</em>}</li>
 *   <li>{@link courses.PreconditionForCourse#isRequiredCourse <em>Required Course</em>}</li>
 *   <li>{@link courses.PreconditionForCourse#getCourse <em>Course</em>}</li>
 *   <li>{@link courses.PreconditionForCourse#getSimilarCourse <em>Similar Course</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getPreconditionForCourse()
 * @model
 * @generated
 */
public interface PreconditionForCourse extends EObject {
	/**
	 * Returns the value of the '<em><b>Credit Reduction</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credit Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credit Reduction</em>' attribute.
	 * @see #setCreditReduction(double)
	 * @see courses.CoursesPackage#getPreconditionForCourse_CreditReduction()
	 * @model default="0"
	 * @generated
	 */
	double getCreditReduction();

	/**
	 * Sets the value of the '{@link courses.PreconditionForCourse#getCreditReduction <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credit Reduction</em>' attribute.
	 * @see #getCreditReduction()
	 * @generated
	 */
	void setCreditReduction(double value);

	/**
	 * Returns the value of the '<em><b>Recommended Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended Course</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended Course</em>' attribute.
	 * @see #setRecommendedCourse(boolean)
	 * @see courses.CoursesPackage#getPreconditionForCourse_RecommendedCourse()
	 * @model
	 * @generated
	 */
	boolean isRecommendedCourse();

	/**
	 * Sets the value of the '{@link courses.PreconditionForCourse#isRecommendedCourse <em>Recommended Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recommended Course</em>' attribute.
	 * @see #isRecommendedCourse()
	 * @generated
	 */
	void setRecommendedCourse(boolean value);

	/**
	 * Returns the value of the '<em><b>Required Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Course</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Course</em>' attribute.
	 * @see #setRequiredCourse(boolean)
	 * @see courses.CoursesPackage#getPreconditionForCourse_RequiredCourse()
	 * @model
	 * @generated
	 */
	boolean isRequiredCourse();

	/**
	 * Sets the value of the '{@link courses.PreconditionForCourse#isRequiredCourse <em>Required Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Course</em>' attribute.
	 * @see #isRequiredCourse()
	 * @generated
	 */
	void setRequiredCourse(boolean value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference list.
	 * The list contents are of type {@link courses.Course}.
	 * It is bidirectional and its opposite is '{@link courses.Course#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference list.
	 * @see courses.CoursesPackage#getPreconditionForCourse_Course()
	 * @see courses.Course#getPrecondition
	 * @model opposite="Precondition"
	 * @generated
	 */
	EList<Course> getCourse();

	/**
	 * Returns the value of the '<em><b>Similar Course</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Boolean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Similar Course</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Similar Course</em>' attribute list.
	 * @see courses.CoursesPackage#getPreconditionForCourse_SimilarCourse()
	 * @model
	 * @generated
	 */
	EList<Boolean> getSimilarCourse();

} // PreconditionForCourse
