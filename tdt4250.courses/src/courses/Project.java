/**
 */
package courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Project#getProjectWeight <em>Project Weight</em>}</li>
 *   <li>{@link courses.Project#getCourseInstance <em>Course Instance</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends EObject {
	/**
	 * Returns the value of the '<em><b>Project Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Weight</em>' attribute.
	 * @see #setProjectWeight(double)
	 * @see courses.CoursesPackage#getProject_ProjectWeight()
	 * @model
	 * @generated
	 */
	double getProjectWeight();

	/**
	 * Sets the value of the '{@link courses.Project#getProjectWeight <em>Project Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Weight</em>' attribute.
	 * @see #getProjectWeight()
	 * @generated
	 */
	void setProjectWeight(double value);

	/**
	 * Returns the value of the '<em><b>Course Instance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instance</em>' container reference.
	 * @see #setCourseInstance(CourseInstance)
	 * @see courses.CoursesPackage#getProject_CourseInstance()
	 * @see courses.CourseInstance#getProject
	 * @model opposite="project" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourseInstance();

	/**
	 * Sets the value of the '{@link courses.Project#getCourseInstance <em>Course Instance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Instance</em>' container reference.
	 * @see #getCourseInstance()
	 * @generated
	 */
	void setCourseInstance(CourseInstance value);

} // Project
