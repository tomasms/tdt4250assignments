/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.StudyProgram#getStudent <em>Student</em>}</li>
 *   <li>{@link courses.StudyProgram#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getStudyProgram()
 * @model
 * @generated
 */
public interface StudyProgram extends Named, Code {
	/**
	 * Returns the value of the '<em><b>Student</b></em>' reference list.
	 * The list contents are of type {@link courses.PersonAtUniversity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Student</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student</em>' reference list.
	 * @see courses.CoursesPackage#getStudyProgram_Student()
	 * @model
	 * @generated
	 */
	EList<PersonAtUniversity> getStudent();

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference list.
	 * The list contents are of type {@link courses.Course}.
	 * It is bidirectional and its opposite is '{@link courses.Course#getStudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference list.
	 * @see courses.CoursesPackage#getStudyProgram_Course()
	 * @see courses.Course#getStudyProgram
	 * @model opposite="studyProgram"
	 * @generated
	 */
	EList<Course> getCourse();

} // StudyProgram
