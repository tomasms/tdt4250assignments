/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.Timetable#getCourse <em>Course</em>}</li>
 *   <li>{@link courses.Timetable#getRoom <em>Room</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getTimetable()
 * @model
 * @generated
 */
public interface Timetable extends Coursework {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link courses.CourseInstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(CourseInstance)
	 * @see courses.CoursesPackage#getTimetable_Course()
	 * @see courses.CourseInstance#getTimetable
	 * @model opposite="timetable" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourse();

	/**
	 * Sets the value of the '{@link courses.Timetable#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(CourseInstance value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute list.
	 * @see courses.CoursesPackage#getTimetable_Room()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getRoom();

} // Timetable
