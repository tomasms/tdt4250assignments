/**
 */
package courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link courses.University#getDepartment <em>Department</em>}</li>
 *   <li>{@link courses.University#getPerson <em>Person</em>}</li>
 * </ul>
 *
 * @see courses.CoursesPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends Named {
	/**
	 * Returns the value of the '<em><b>Department</b></em>' containment reference list.
	 * The list contents are of type {@link courses.Department}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' containment reference list.
	 * @see courses.CoursesPackage#getUniversity_Department()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Department> getDepartment();

	/**
	 * Returns the value of the '<em><b>Person</b></em>' containment reference list.
	 * The list contents are of type {@link courses.PersonAtUniversity}.
	 * It is bidirectional and its opposite is '{@link courses.PersonAtUniversity#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' containment reference list.
	 * @see courses.CoursesPackage#getUniversity_Person()
	 * @see courses.PersonAtUniversity#getUniversity
	 * @model opposite="University" containment="true" required="true"
	 * @generated
	 */
	EList<PersonAtUniversity> getPerson();

} // University
