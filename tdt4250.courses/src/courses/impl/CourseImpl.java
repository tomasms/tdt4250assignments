/**
 */
package courses.impl;

import courses.Code;
import courses.Course;
import courses.CourseInstance;
import courses.CoursesPackage;
import courses.Coursework;
import courses.PreconditionForCourse;
import courses.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getCourseInstance <em>Course Instance</em>}</li>
 *   <li>{@link courses.impl.CourseImpl#getPrecondition <em>Precondition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> lectureHours;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> labHours;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudyProgram() <em>Study Program</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgram()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyProgram;

	/**
	 * The cached value of the '{@link #getCourseInstance() <em>Course Instance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseInstance;

	/**
	 * The cached value of the '{@link #getPrecondition() <em>Precondition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecondition()
	 * @generated
	 * @ordered
	 */
	protected EList<PreconditionForCourse> precondition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getLectureHours() {
		if (lectureHours == null) {
			lectureHours = new EDataTypeUniqueEList<Integer>(Integer.class, this, CoursesPackage.COURSE__LECTURE_HOURS);
		}
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getLabHours() {
		if (labHours == null) {
			labHours = new EDataTypeUniqueEList<Integer>(Integer.class, this, CoursesPackage.COURSE__LAB_HOURS);
		}
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyProgram() {
		if (studyProgram == null) {
			studyProgram = new EObjectWithInverseResolvingEList.ManyInverse<StudyProgram>(StudyProgram.class, this, CoursesPackage.COURSE__STUDY_PROGRAM, CoursesPackage.STUDY_PROGRAM__COURSE);
		}
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseInstance() {
		if (courseInstance == null) {
			courseInstance = new EObjectContainmentWithInverseEList<CourseInstance>(CourseInstance.class, this, CoursesPackage.COURSE__COURSE_INSTANCE, CoursesPackage.COURSE_INSTANCE__COURSE);
		}
		return courseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreconditionForCourse> getPrecondition() {
		if (precondition == null) {
			precondition = new EObjectWithInverseResolvingEList.ManyInverse<PreconditionForCourse>(PreconditionForCourse.class, this, CoursesPackage.COURSE__PRECONDITION, CoursesPackage.PRECONDITION_FOR_COURSE__COURSE);
		}
		return precondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStudyProgram()).basicAdd(otherEnd, msgs);
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseInstance()).basicAdd(otherEnd, msgs);
			case CoursesPackage.COURSE__PRECONDITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPrecondition()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				return ((InternalEList<?>)getStudyProgram()).basicRemove(otherEnd, msgs);
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				return ((InternalEList<?>)getCourseInstance()).basicRemove(otherEnd, msgs);
			case CoursesPackage.COURSE__PRECONDITION:
				return ((InternalEList<?>)getPrecondition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.COURSE__NAME:
				return getName();
			case CoursesPackage.COURSE__CODE:
				return getCode();
			case CoursesPackage.COURSE__LECTURE_HOURS:
				return getLectureHours();
			case CoursesPackage.COURSE__LAB_HOURS:
				return getLabHours();
			case CoursesPackage.COURSE__CONTENT:
				return getContent();
			case CoursesPackage.COURSE__CREDITS:
				return getCredits();
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				return getStudyProgram();
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				return getCourseInstance();
			case CoursesPackage.COURSE__PRECONDITION:
				return getPrecondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.COURSE__NAME:
				setName((String)newValue);
				return;
			case CoursesPackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case CoursesPackage.COURSE__LECTURE_HOURS:
				getLectureHours().clear();
				getLectureHours().addAll((Collection<? extends Integer>)newValue);
				return;
			case CoursesPackage.COURSE__LAB_HOURS:
				getLabHours().clear();
				getLabHours().addAll((Collection<? extends Integer>)newValue);
				return;
			case CoursesPackage.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case CoursesPackage.COURSE__CREDITS:
				setCredits((Double)newValue);
				return;
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				getStudyProgram().clear();
				getStudyProgram().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				getCourseInstance().clear();
				getCourseInstance().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case CoursesPackage.COURSE__PRECONDITION:
				getPrecondition().clear();
				getPrecondition().addAll((Collection<? extends PreconditionForCourse>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursesPackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case CoursesPackage.COURSE__LECTURE_HOURS:
				getLectureHours().clear();
				return;
			case CoursesPackage.COURSE__LAB_HOURS:
				getLabHours().clear();
				return;
			case CoursesPackage.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case CoursesPackage.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				getStudyProgram().clear();
				return;
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				getCourseInstance().clear();
				return;
			case CoursesPackage.COURSE__PRECONDITION:
				getPrecondition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursesPackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case CoursesPackage.COURSE__LECTURE_HOURS:
				return lectureHours != null && !lectureHours.isEmpty();
			case CoursesPackage.COURSE__LAB_HOURS:
				return labHours != null && !labHours.isEmpty();
			case CoursesPackage.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case CoursesPackage.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case CoursesPackage.COURSE__STUDY_PROGRAM:
				return studyProgram != null && !studyProgram.isEmpty();
			case CoursesPackage.COURSE__COURSE_INSTANCE:
				return courseInstance != null && !courseInstance.isEmpty();
			case CoursesPackage.COURSE__PRECONDITION:
				return precondition != null && !precondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Code.class) {
			switch (derivedFeatureID) {
				case CoursesPackage.COURSE__CODE: return CoursesPackage.CODE__CODE;
				default: return -1;
			}
		}
		if (baseClass == Coursework.class) {
			switch (derivedFeatureID) {
				case CoursesPackage.COURSE__LECTURE_HOURS: return CoursesPackage.COURSEWORK__LECTURE_HOURS;
				case CoursesPackage.COURSE__LAB_HOURS: return CoursesPackage.COURSEWORK__LAB_HOURS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Code.class) {
			switch (baseFeatureID) {
				case CoursesPackage.CODE__CODE: return CoursesPackage.COURSE__CODE;
				default: return -1;
			}
		}
		if (baseClass == Coursework.class) {
			switch (baseFeatureID) {
				case CoursesPackage.COURSEWORK__LECTURE_HOURS: return CoursesPackage.COURSE__LECTURE_HOURS;
				case CoursesPackage.COURSEWORK__LAB_HOURS: return CoursesPackage.COURSE__LAB_HOURS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", code: ");
		result.append(code);
		result.append(", lectureHours: ");
		result.append(lectureHours);
		result.append(", labHours: ");
		result.append(labHours);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
