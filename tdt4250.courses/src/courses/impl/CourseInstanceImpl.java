/**
 */
package courses.impl;

import courses.Assignment;
import courses.Course;
import courses.CourseInstance;
import courses.CoursesPackage;
import courses.Exam;
import courses.PersonAtUniversity;
import courses.Project;
import courses.Timetable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.CourseInstanceImpl#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getCourseAffiliation <em>Course Affiliation</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getExam <em>Exam</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getProject <em>Project</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getAssignment <em>Assignment</em>}</li>
 *   <li>{@link courses.impl.CourseInstanceImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected Timetable timetable;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseAffiliation() <em>Course Affiliation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseAffiliation()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonAtUniversity> courseAffiliation;

	/**
	 * The cached value of the '{@link #getExam() <em>Exam</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExam()
	 * @generated
	 * @ordered
	 */
	protected Exam exam;

	/**
	 * The cached value of the '{@link #getProject() <em>Project</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject()
	 * @generated
	 * @ordered
	 */
	protected Project project;

	/**
	 * The cached value of the '{@link #getAssignment() <em>Assignment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignment()
	 * @generated
	 * @ordered
	 */
	protected Assignment assignment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable getTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimetable(Timetable newTimetable, NotificationChain msgs) {
		Timetable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__TIMETABLE, oldTimetable, newTimetable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(Timetable newTimetable) {
		if (newTimetable != timetable) {
			NotificationChain msgs = null;
			if (timetable != null)
				msgs = ((InternalEObject)timetable).eInverseRemove(this, CoursesPackage.TIMETABLE__COURSE, Timetable.class, msgs);
			if (newTimetable != null)
				msgs = ((InternalEObject)newTimetable).eInverseAdd(this, CoursesPackage.TIMETABLE__COURSE, Timetable.class, msgs);
			msgs = basicSetTimetable(newTimetable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__TIMETABLE, newTimetable, newTimetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonAtUniversity> getCourseAffiliation() {
		if (courseAffiliation == null) {
			courseAffiliation = new EObjectWithInverseResolvingEList.ManyInverse<PersonAtUniversity>(PersonAtUniversity.class, this, CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION, CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION);
		}
		return courseAffiliation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exam getExam() {
		return exam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExam(Exam newExam, NotificationChain msgs) {
		Exam oldExam = exam;
		exam = newExam;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__EXAM, oldExam, newExam);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExam(Exam newExam) {
		if (newExam != exam) {
			NotificationChain msgs = null;
			if (exam != null)
				msgs = ((InternalEObject)exam).eInverseRemove(this, CoursesPackage.EXAM__COURSE_INSTANCE, Exam.class, msgs);
			if (newExam != null)
				msgs = ((InternalEObject)newExam).eInverseAdd(this, CoursesPackage.EXAM__COURSE_INSTANCE, Exam.class, msgs);
			msgs = basicSetExam(newExam, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__EXAM, newExam, newExam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProject(Project newProject, NotificationChain msgs) {
		Project oldProject = project;
		project = newProject;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__PROJECT, oldProject, newProject);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject(Project newProject) {
		if (newProject != project) {
			NotificationChain msgs = null;
			if (project != null)
				msgs = ((InternalEObject)project).eInverseRemove(this, CoursesPackage.PROJECT__COURSE_INSTANCE, Project.class, msgs);
			if (newProject != null)
				msgs = ((InternalEObject)newProject).eInverseAdd(this, CoursesPackage.PROJECT__COURSE_INSTANCE, Project.class, msgs);
			msgs = basicSetProject(newProject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__PROJECT, newProject, newProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment getAssignment() {
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssignment(Assignment newAssignment, NotificationChain msgs) {
		Assignment oldAssignment = assignment;
		assignment = newAssignment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__ASSIGNMENT, oldAssignment, newAssignment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignment(Assignment newAssignment) {
		if (newAssignment != assignment) {
			NotificationChain msgs = null;
			if (assignment != null)
				msgs = ((InternalEObject)assignment).eInverseRemove(this, CoursesPackage.ASSIGNMENT__COURSE_INSTANCE, Assignment.class, msgs);
			if (newAssignment != null)
				msgs = ((InternalEObject)newAssignment).eInverseAdd(this, CoursesPackage.ASSIGNMENT__COURSE_INSTANCE, Assignment.class, msgs);
			msgs = basicSetAssignment(newAssignment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__ASSIGNMENT, newAssignment, newAssignment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != CoursesPackage.COURSE_INSTANCE__COURSE) return null;
		return (Course)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCourse, CoursesPackage.COURSE_INSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer() || (eContainerFeatureID() != CoursesPackage.COURSE_INSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject)newCourse).eInverseAdd(this, CoursesPackage.COURSE__COURSE_INSTANCE, Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_INSTANCE__COURSE, newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				if (timetable != null)
					msgs = ((InternalEObject)timetable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSE_INSTANCE__TIMETABLE, null, msgs);
				return basicSetTimetable((Timetable)otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseAffiliation()).basicAdd(otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				if (exam != null)
					msgs = ((InternalEObject)exam).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSE_INSTANCE__EXAM, null, msgs);
				return basicSetExam((Exam)otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				if (project != null)
					msgs = ((InternalEObject)project).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSE_INSTANCE__PROJECT, null, msgs);
				return basicSetProject((Project)otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				if (assignment != null)
					msgs = ((InternalEObject)assignment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSE_INSTANCE__ASSIGNMENT, null, msgs);
				return basicSetAssignment((Assignment)otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCourse((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return basicSetTimetable(null, msgs);
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				return ((InternalEList<?>)getCourseAffiliation()).basicRemove(otherEnd, msgs);
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				return basicSetExam(null, msgs);
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				return basicSetProject(null, msgs);
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				return basicSetAssignment(null, msgs);
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				return basicSetCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSE__COURSE_INSTANCE, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return getTimetable();
			case CoursesPackage.COURSE_INSTANCE__YEAR:
				return getYear();
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				return getCourseAffiliation();
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				return getExam();
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				return getProject();
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				return getAssignment();
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				setTimetable((Timetable)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__YEAR:
				setYear((Integer)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				getCourseAffiliation().clear();
				getCourseAffiliation().addAll((Collection<? extends PersonAtUniversity>)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				setExam((Exam)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				setProject((Project)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				setAssignment((Assignment)newValue);
				return;
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				setCourse((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				setTimetable((Timetable)null);
				return;
			case CoursesPackage.COURSE_INSTANCE__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				getCourseAffiliation().clear();
				return;
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				setExam((Exam)null);
				return;
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				setProject((Project)null);
				return;
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				setAssignment((Assignment)null);
				return;
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				setCourse((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return timetable != null;
			case CoursesPackage.COURSE_INSTANCE__YEAR:
				return year != YEAR_EDEFAULT;
			case CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION:
				return courseAffiliation != null && !courseAffiliation.isEmpty();
			case CoursesPackage.COURSE_INSTANCE__EXAM:
				return exam != null;
			case CoursesPackage.COURSE_INSTANCE__PROJECT:
				return project != null;
			case CoursesPackage.COURSE_INSTANCE__ASSIGNMENT:
				return assignment != null;
			case CoursesPackage.COURSE_INSTANCE__COURSE:
				return getCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Year: ");
		result.append(year);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
