/**
 */
package courses.impl;

import courses.CoursesPackage;
import courses.Coursework;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.CourseworkImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link courses.impl.CourseworkImpl#getLabHours <em>Lab Hours</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CourseworkImpl extends MinimalEObjectImpl.Container implements Coursework {
	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> lectureHours;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> labHours;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSEWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getLectureHours() {
		if (lectureHours == null) {
			lectureHours = new EDataTypeUniqueEList<Integer>(Integer.class, this, CoursesPackage.COURSEWORK__LECTURE_HOURS);
		}
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getLabHours() {
		if (labHours == null) {
			labHours = new EDataTypeUniqueEList<Integer>(Integer.class, this, CoursesPackage.COURSEWORK__LAB_HOURS);
		}
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.COURSEWORK__LECTURE_HOURS:
				return getLectureHours();
			case CoursesPackage.COURSEWORK__LAB_HOURS:
				return getLabHours();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.COURSEWORK__LECTURE_HOURS:
				getLectureHours().clear();
				getLectureHours().addAll((Collection<? extends Integer>)newValue);
				return;
			case CoursesPackage.COURSEWORK__LAB_HOURS:
				getLabHours().clear();
				getLabHours().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSEWORK__LECTURE_HOURS:
				getLectureHours().clear();
				return;
			case CoursesPackage.COURSEWORK__LAB_HOURS:
				getLabHours().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.COURSEWORK__LECTURE_HOURS:
				return lectureHours != null && !lectureHours.isEmpty();
			case CoursesPackage.COURSEWORK__LAB_HOURS:
				return labHours != null && !labHours.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lectureHours: ");
		result.append(lectureHours);
		result.append(", labHours: ");
		result.append(labHours);
		result.append(')');
		return result.toString();
	}

} //CourseworkImpl
