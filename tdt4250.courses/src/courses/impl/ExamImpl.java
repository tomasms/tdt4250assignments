/**
 */
package courses.impl;

import courses.CourseInstance;
import courses.CoursesPackage;
import courses.Exam;
import courses.PersonAtUniversity;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exam</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.ExamImpl#getExamWeight <em>Exam Weight</em>}</li>
 *   <li>{@link courses.impl.ExamImpl#getPersonTakingExam <em>Person Taking Exam</em>}</li>
 *   <li>{@link courses.impl.ExamImpl#getCourseInstance <em>Course Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExamImpl extends MinimalEObjectImpl.Container implements Exam {
	/**
	 * The default value of the '{@link #getExamWeight() <em>Exam Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExamWeight()
	 * @generated
	 * @ordered
	 */
	protected static final double EXAM_WEIGHT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getExamWeight() <em>Exam Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExamWeight()
	 * @generated
	 * @ordered
	 */
	protected double examWeight = EXAM_WEIGHT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPersonTakingExam() <em>Person Taking Exam</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonTakingExam()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonAtUniversity> personTakingExam;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.EXAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getExamWeight() {
		return examWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExamWeight(double newExamWeight) {
		double oldExamWeight = examWeight;
		examWeight = newExamWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EXAM__EXAM_WEIGHT, oldExamWeight, examWeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonAtUniversity> getPersonTakingExam() {
		if (personTakingExam == null) {
			personTakingExam = new EObjectWithInverseResolvingEList.ManyInverse<PersonAtUniversity>(PersonAtUniversity.class, this, CoursesPackage.EXAM__PERSON_TAKING_EXAM, CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM);
		}
		return personTakingExam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourseInstance() {
		if (eContainerFeatureID() != CoursesPackage.EXAM__COURSE_INSTANCE) return null;
		return (CourseInstance)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseInstance(CourseInstance newCourseInstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCourseInstance, CoursesPackage.EXAM__COURSE_INSTANCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseInstance(CourseInstance newCourseInstance) {
		if (newCourseInstance != eInternalContainer() || (eContainerFeatureID() != CoursesPackage.EXAM__COURSE_INSTANCE && newCourseInstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseInstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseInstance != null)
				msgs = ((InternalEObject)newCourseInstance).eInverseAdd(this, CoursesPackage.COURSE_INSTANCE__EXAM, CourseInstance.class, msgs);
			msgs = basicSetCourseInstance(newCourseInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EXAM__COURSE_INSTANCE, newCourseInstance, newCourseInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPersonTakingExam()).basicAdd(otherEnd, msgs);
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCourseInstance((CourseInstance)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				return ((InternalEList<?>)getPersonTakingExam()).basicRemove(otherEnd, msgs);
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				return basicSetCourseInstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSE_INSTANCE__EXAM, CourseInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.EXAM__EXAM_WEIGHT:
				return getExamWeight();
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				return getPersonTakingExam();
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				return getCourseInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.EXAM__EXAM_WEIGHT:
				setExamWeight((Double)newValue);
				return;
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				getPersonTakingExam().clear();
				getPersonTakingExam().addAll((Collection<? extends PersonAtUniversity>)newValue);
				return;
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				setCourseInstance((CourseInstance)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.EXAM__EXAM_WEIGHT:
				setExamWeight(EXAM_WEIGHT_EDEFAULT);
				return;
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				getPersonTakingExam().clear();
				return;
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				setCourseInstance((CourseInstance)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.EXAM__EXAM_WEIGHT:
				return examWeight != EXAM_WEIGHT_EDEFAULT;
			case CoursesPackage.EXAM__PERSON_TAKING_EXAM:
				return personTakingExam != null && !personTakingExam.isEmpty();
			case CoursesPackage.EXAM__COURSE_INSTANCE:
				return getCourseInstance() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ExamWeight: ");
		result.append(examWeight);
		result.append(')');
		return result.toString();
	}

} //ExamImpl
