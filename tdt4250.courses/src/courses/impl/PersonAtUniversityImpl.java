/**
 */
package courses.impl;

import courses.CourseInstance;
import courses.CoursesPackage;
import courses.Exam;
import courses.PersonAtUniversity;
import courses.Role;
import courses.University;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person At University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getName <em>Name</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getUniversity <em>University</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getCourseAffiliation <em>Course Affiliation</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getTotalCredits <em>Total Credits</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getRole <em>Role</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getTakeExam <em>Take Exam</em>}</li>
 *   <li>{@link courses.impl.PersonAtUniversityImpl#getPassedExams <em>Passed Exams</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonAtUniversityImpl extends MinimalEObjectImpl.Container implements PersonAtUniversity {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	

	
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseAffiliation() <em>Course Affiliation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseAffiliation()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseAffiliation;

	/**
	 * The default value of the '{@link #getTotalCredits() <em>Total Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_CREDITS_EDEFAULT = 0.0;
	
	
	protected double totalCredits = TOTAL_CREDITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected static final Role ROLE_EDEFAULT = Role.LECTURER;



	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected Role role = ROLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTakeExam() <em>Take Exam</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTakeExam()
	 * @generated
	 * @ordered
	 */
	protected EList<Exam> takeExam;

	/**
	 * The cached value of the '{@link #getPassedExams() <em>Passed Exams</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassedExams()
	 * @generated
	 * @ordered
	 */
	protected EList<Exam> passedExams;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonAtUniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.PERSON_AT_UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PERSON_AT_UNIVERSITY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public University getUniversity() {
		if (eContainerFeatureID() != CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY) return null;
		return (University)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversity(University newUniversity, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newUniversity, CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversity(University newUniversity) {
		if (newUniversity != eInternalContainer() || (eContainerFeatureID() != CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY && newUniversity != null)) {
			if (EcoreUtil.isAncestor(this, newUniversity))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newUniversity != null)
				msgs = ((InternalEObject)newUniversity).eInverseAdd(this, CoursesPackage.UNIVERSITY__PERSON, University.class, msgs);
			msgs = basicSetUniversity(newUniversity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY, newUniversity, newUniversity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(Role newRole) {
		Role oldRole = role;
		role = newRole == null ? ROLE_EDEFAULT : newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PERSON_AT_UNIVERSITY__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Exam> getTakeExam() {
		if (takeExam == null) {
			takeExam = new EObjectWithInverseResolvingEList.ManyInverse<Exam>(Exam.class, this, CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM, CoursesPackage.EXAM__PERSON_TAKING_EXAM);
		}
		return takeExam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Exam> getPassedExams() {
		if (passedExams == null) {
			passedExams = new EObjectResolvingEList<Exam>(Exam.class, this, CoursesPackage.PERSON_AT_UNIVERSITY__PASSED_EXAMS);
		}
		return passedExams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void signUpForExam(Exam exam) {	
		if(getTakeExam().contains(exam)) {
			throw new UnsupportedOperationException();
	}
		getTakeExam().add(exam);

		//getRegistredExam().add(exam);
		// Ensure that you remove @generated or mark it @generated NOT
}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseAffiliation() {
		if (courseAffiliation == null) {
			courseAffiliation = new EObjectWithInverseResolvingEList.ManyInverse<CourseInstance>(CourseInstance.class, this, CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION, CoursesPackage.COURSE_INSTANCE__COURSE_AFFILIATION);
		}
		return courseAffiliation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getTotalCredits() {
		return totalCredits;
		// TODO: implement this method to return the 'Total Credits' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		//throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelExam(Exam exam) {
		if(!getTakeExam().contains(exam)) {
			throw new UnsupportedOperationException();
		}
		getTakeExam().remove(exam);
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT


	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void takingExam(Exam exam) {
		if(!getTakeExam().contains(exam)) {
			throw new UnsupportedOperationException();
		}
		//Antar at man kan ta eksamen selv om man har tatt den f�r.
		getTakeExam().remove(exam);
		getPassedExams().add(exam);
		totalCredits += exam.getCourseInstance().getCourse().getCredits();
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		
	}
	

	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetUniversity((University)otherEnd, msgs);
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseAffiliation()).basicAdd(otherEnd, msgs);
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTakeExam()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				return basicSetUniversity(null, msgs);
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				return ((InternalEList<?>)getCourseAffiliation()).basicRemove(otherEnd, msgs);
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				return ((InternalEList<?>)getTakeExam()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				return eInternalContainer().eInverseRemove(this, CoursesPackage.UNIVERSITY__PERSON, University.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__NAME:
				return getName();
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				return getUniversity();
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				return getCourseAffiliation();
			case CoursesPackage.PERSON_AT_UNIVERSITY__TOTAL_CREDITS:
				return getTotalCredits();
			case CoursesPackage.PERSON_AT_UNIVERSITY__ROLE:
				return getRole();
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				return getTakeExam();
			case CoursesPackage.PERSON_AT_UNIVERSITY__PASSED_EXAMS:
				return getPassedExams();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__NAME:
				setName((String)newValue);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				setUniversity((University)newValue);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				getCourseAffiliation().clear();
				getCourseAffiliation().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__ROLE:
				setRole((Role)newValue);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				getTakeExam().clear();
				getTakeExam().addAll((Collection<? extends Exam>)newValue);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__PASSED_EXAMS:
				getPassedExams().clear();
				getPassedExams().addAll((Collection<? extends Exam>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				setUniversity((University)null);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				getCourseAffiliation().clear();
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__ROLE:
				setRole(ROLE_EDEFAULT);
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				getTakeExam().clear();
				return;
			case CoursesPackage.PERSON_AT_UNIVERSITY__PASSED_EXAMS:
				getPassedExams().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursesPackage.PERSON_AT_UNIVERSITY__UNIVERSITY:
				return getUniversity() != null;
			case CoursesPackage.PERSON_AT_UNIVERSITY__COURSE_AFFILIATION:
				return courseAffiliation != null && !courseAffiliation.isEmpty();
			case CoursesPackage.PERSON_AT_UNIVERSITY__TOTAL_CREDITS:
				return getTotalCredits() != TOTAL_CREDITS_EDEFAULT;
			case CoursesPackage.PERSON_AT_UNIVERSITY__ROLE:
				return role != ROLE_EDEFAULT;
			case CoursesPackage.PERSON_AT_UNIVERSITY__TAKE_EXAM:
				return takeExam != null && !takeExam.isEmpty();
			case CoursesPackage.PERSON_AT_UNIVERSITY__PASSED_EXAMS:
				return passedExams != null && !passedExams.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CoursesPackage.PERSON_AT_UNIVERSITY___SIGN_UP_FOR_EXAM__EXAM:
				signUpForExam((Exam)arguments.get(0));
				return null;
			case CoursesPackage.PERSON_AT_UNIVERSITY___CANCEL_EXAM__EXAM:
				cancelExam((Exam)arguments.get(0));
				return null;
			case CoursesPackage.PERSON_AT_UNIVERSITY___TAKING_EXAM__EXAM:
				takingExam((Exam)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", Role: ");
		result.append(role);
		result.append(')');
		return result.toString();
	}






	








} //PersonAtUniversityImpl
