/**
 */
package courses.impl;

import courses.Course;
import courses.CoursesPackage;
import courses.PreconditionForCourse;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Precondition For Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link courses.impl.PreconditionForCourseImpl#getCreditReduction <em>Credit Reduction</em>}</li>
 *   <li>{@link courses.impl.PreconditionForCourseImpl#isRecommendedCourse <em>Recommended Course</em>}</li>
 *   <li>{@link courses.impl.PreconditionForCourseImpl#isRequiredCourse <em>Required Course</em>}</li>
 *   <li>{@link courses.impl.PreconditionForCourseImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link courses.impl.PreconditionForCourseImpl#getSimilarCourse <em>Similar Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PreconditionForCourseImpl extends MinimalEObjectImpl.Container implements PreconditionForCourse {
	/**
	 * The default value of the '{@link #getCreditReduction() <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReduction()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDIT_REDUCTION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCreditReduction() <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReduction()
	 * @generated
	 * @ordered
	 */
	protected double creditReduction = CREDIT_REDUCTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isRecommendedCourse() <em>Recommended Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecommendedCourse()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RECOMMENDED_COURSE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRecommendedCourse() <em>Recommended Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecommendedCourse()
	 * @generated
	 * @ordered
	 */
	protected boolean recommendedCourse = RECOMMENDED_COURSE_EDEFAULT;

	/**
	 * The default value of the '{@link #isRequiredCourse() <em>Required Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequiredCourse()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REQUIRED_COURSE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRequiredCourse() <em>Required Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequiredCourse()
	 * @generated
	 * @ordered
	 */
	protected boolean requiredCourse = REQUIRED_COURSE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> course;

	/**
	 * The cached value of the '{@link #getSimilarCourse() <em>Similar Course</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimilarCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Boolean> similarCourse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PreconditionForCourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.PRECONDITION_FOR_COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCreditReduction() {
		return creditReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreditReduction(double newCreditReduction) {
		double oldCreditReduction = creditReduction;
		creditReduction = newCreditReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PRECONDITION_FOR_COURSE__CREDIT_REDUCTION, oldCreditReduction, creditReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRecommendedCourse() {
		return recommendedCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecommendedCourse(boolean newRecommendedCourse) {
		boolean oldRecommendedCourse = recommendedCourse;
		recommendedCourse = newRecommendedCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE, oldRecommendedCourse, recommendedCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRequiredCourse() {
		return requiredCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredCourse(boolean newRequiredCourse) {
		boolean oldRequiredCourse = requiredCourse;
		requiredCourse = newRequiredCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.PRECONDITION_FOR_COURSE__REQUIRED_COURSE, oldRequiredCourse, requiredCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourse() {
		if (course == null) {
			course = new EObjectWithInverseResolvingEList.ManyInverse<Course>(Course.class, this, CoursesPackage.PRECONDITION_FOR_COURSE__COURSE, CoursesPackage.COURSE__PRECONDITION);
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Boolean> getSimilarCourse() {
		if (similarCourse == null) {
			similarCourse = new EDataTypeUniqueEList<Boolean>(Boolean.class, this, CoursesPackage.PRECONDITION_FOR_COURSE__SIMILAR_COURSE);
		}
		return similarCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourse()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				return ((InternalEList<?>)getCourse()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__CREDIT_REDUCTION:
				return getCreditReduction();
			case CoursesPackage.PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE:
				return isRecommendedCourse();
			case CoursesPackage.PRECONDITION_FOR_COURSE__REQUIRED_COURSE:
				return isRequiredCourse();
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				return getCourse();
			case CoursesPackage.PRECONDITION_FOR_COURSE__SIMILAR_COURSE:
				return getSimilarCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__CREDIT_REDUCTION:
				setCreditReduction((Double)newValue);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE:
				setRecommendedCourse((Boolean)newValue);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__REQUIRED_COURSE:
				setRequiredCourse((Boolean)newValue);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				getCourse().clear();
				getCourse().addAll((Collection<? extends Course>)newValue);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__SIMILAR_COURSE:
				getSimilarCourse().clear();
				getSimilarCourse().addAll((Collection<? extends Boolean>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__CREDIT_REDUCTION:
				setCreditReduction(CREDIT_REDUCTION_EDEFAULT);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE:
				setRecommendedCourse(RECOMMENDED_COURSE_EDEFAULT);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__REQUIRED_COURSE:
				setRequiredCourse(REQUIRED_COURSE_EDEFAULT);
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				getCourse().clear();
				return;
			case CoursesPackage.PRECONDITION_FOR_COURSE__SIMILAR_COURSE:
				getSimilarCourse().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursesPackage.PRECONDITION_FOR_COURSE__CREDIT_REDUCTION:
				return creditReduction != CREDIT_REDUCTION_EDEFAULT;
			case CoursesPackage.PRECONDITION_FOR_COURSE__RECOMMENDED_COURSE:
				return recommendedCourse != RECOMMENDED_COURSE_EDEFAULT;
			case CoursesPackage.PRECONDITION_FOR_COURSE__REQUIRED_COURSE:
				return requiredCourse != REQUIRED_COURSE_EDEFAULT;
			case CoursesPackage.PRECONDITION_FOR_COURSE__COURSE:
				return course != null && !course.isEmpty();
			case CoursesPackage.PRECONDITION_FOR_COURSE__SIMILAR_COURSE:
				return similarCourse != null && !similarCourse.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (CreditReduction: ");
		result.append(creditReduction);
		result.append(", recommendedCourse: ");
		result.append(recommendedCourse);
		result.append(", requiredCourse: ");
		result.append(requiredCourse);
		result.append(", similarCourse: ");
		result.append(similarCourse);
		result.append(')');
		return result.toString();
	}

} //PreconditionForCourseImpl
