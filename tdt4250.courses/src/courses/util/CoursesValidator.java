/**
 */
package courses.util;

import courses.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see courses.CoursesPackage
 * @generated
 */
public class CoursesValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CoursesValidator INSTANCE = new CoursesValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "courses";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoursesValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return CoursesPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case CoursesPackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case CoursesPackage.STUDY_PROGRAM:
				return validateStudyProgram((StudyProgram)value, diagnostics, context);
			case CoursesPackage.NAMED:
				return validateNamed((Named)value, diagnostics, context);
			case CoursesPackage.CODE:
				return validateCode((Code)value, diagnostics, context);
			case CoursesPackage.TIMETABLE:
				return validateTimetable((Timetable)value, diagnostics, context);
			case CoursesPackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case CoursesPackage.COURSEWORK:
				return validateCoursework((Coursework)value, diagnostics, context);
			case CoursesPackage.PERSON_AT_UNIVERSITY:
				return validatePersonAtUniversity((PersonAtUniversity)value, diagnostics, context);
			case CoursesPackage.UNIVERSITY:
				return validateUniversity((University)value, diagnostics, context);
			case CoursesPackage.COURSE_INSTANCE:
				return validateCourseInstance((CourseInstance)value, diagnostics, context);
			case CoursesPackage.PRECONDITION_FOR_COURSE:
				return validatePreconditionForCourse((PreconditionForCourse)value, diagnostics, context);
			case CoursesPackage.EXAM:
				return validateExam((Exam)value, diagnostics, context);
			case CoursesPackage.ASSIGNMENT:
				return validateAssignment((Assignment)value, diagnostics, context);
			case CoursesPackage.PROJECT:
				return validateProject((Project)value, diagnostics, context);
			case CoursesPackage.ROLE:
				return validateRole((Role)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyProgram(StudyProgram studyProgram, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(studyProgram, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNamed(Named named, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(named, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCode(Code code, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(code, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimetable(Timetable timetable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timetable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCoursework(Coursework coursework, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(coursework, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonAtUniversity(PersonAtUniversity personAtUniversity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(personAtUniversity, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(personAtUniversity, diagnostics, context);
		if (result || diagnostics != null) result &= validatePersonAtUniversity_checkIfStudent(personAtUniversity, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkIfStudent constraint of '<em>Person At University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validatePersonAtUniversity_checkIfStudent(PersonAtUniversity personAtUniversity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (!isPersonStudent(personAtUniversity)) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "checkIfStudent", getObjectLabel(personAtUniversity, context) },
						 new Object[] { personAtUniversity },
						 context));
			}
			return false;
		}
		return true;
	}

	public boolean isPersonStudent(PersonAtUniversity personAtUniversity) {
		return personAtUniversity.getRole().equals(Role.STUDENT);

	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(university, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_hasCoordinator(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_evaluationSumIs100(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * Validates the hasCoordinator constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_hasCoordinator(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (!hasSingleCoordinator(courseInstance)) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "hasCoordinator", getObjectLabel(courseInstance, context) },
						 new Object[] { courseInstance },
						 context));
			}
			return false;
		}
		return true;
	}
	
	public boolean hasSingleCoordinator(CourseInstance instance) {
		return instance.getCourseAffiliation().stream().anyMatch(p -> p.getRole().equals(Role.COURSE_COORDINATOR));
	}
	
	/**
	 * Validates the evaluationSumIs100 constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_evaluationSumIs100(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (!isSum100(courseInstance)) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "evaluationSumIs100", getObjectLabel(courseInstance, context) },
						 new Object[] { courseInstance },
						 context));
			}
			return false;
		}
		return true;
	}



	public boolean isSum100(CourseInstance instance) {
		if ((instance.getExam().getExamWeight() + instance.getProject().getProjectWeight() + instance.getAssignment().getAssignmentWeight()) == 100 ) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePreconditionForCourse(PreconditionForCourse preconditionForCourse, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(preconditionForCourse, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExam(Exam exam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(exam, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssignment(Assignment assignment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assignment, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProject(Project project, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(project, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //CoursesValidator
