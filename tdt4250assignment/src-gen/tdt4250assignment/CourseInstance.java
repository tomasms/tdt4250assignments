/**
 */
package tdt4250assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250assignment.CourseInstance#getEvaluationForm <em>Evaluation Form</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250assignment.CourseInstance#getPersonInCourse <em>Person In Course</em>}</li>
 * </ul>
 *
 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='percentageSum hasACoordinator timetableCorrespondsToCoursework'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 percentageSum='self.evaluationForm.percentage-&gt;sum() == 100' hasACoordinator='self.staff.type-&gt;select(item | item == \"coordinator\")-&gt;length == 1'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Evaluation Form</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.Work}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation Form</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Form</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_EvaluationForm()
	 * @model containment="true"
	 * @generated
	 */
	EList<Work> getEvaluationForm();

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_Year()
	 * @model
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link tdt4250assignment.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Lab Hours</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.ScheduledHour}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab Hours</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab Hours</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_LabHours()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScheduledHour> getLabHours();

	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.ScheduledHour}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_LectureHours()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScheduledHour> getLectureHours();

	/**
	 * Returns the value of the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Work</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Work</em>' containment reference.
	 * @see #setCourseWork(CourseWork)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_CourseWork()
	 * @model containment="true"
	 * @generated
	 */
	CourseWork getCourseWork();

	/**
	 * Sets the value of the '{@link tdt4250assignment.CourseInstance#getCourseWork <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Work</em>' containment reference.
	 * @see #getCourseWork()
	 * @generated
	 */
	void setCourseWork(CourseWork value);

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' reference list.
	 * The list contents are of type {@link tdt4250assignment.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Programs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_StudyPrograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgram> getStudyPrograms();

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250assignment.Course#getInstances <em>Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_Course()
	 * @see tdt4250assignment.Course#getInstances
	 * @model opposite="instances" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250assignment.CourseInstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Person In Course</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.PersonInCourse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person In Course</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person In Course</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getCourseInstance_PersonInCourse()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<PersonInCourse> getPersonInCourse();

} // CourseInstance
