/**
 */
package tdt4250assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250assignment.Department#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250assignment.Department#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250assignment.Department#getPersonAffiliatedWithDepartment <em>Person Affiliated With Department</em>}</li>
 * </ul>
 *
 * @see tdt4250assignment.Tdt4250assignmentPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getDepartment_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250assignment.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250assignment.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getDepartment_Courses()
	 * @see tdt4250assignment.Course#getDepartment
	 * @model opposite="department" containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Person Affiliated With Department</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250assignment.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Affiliated With Department</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Affiliated With Department</em>' containment reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getDepartment_PersonAffiliatedWithDepartment()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getPersonAffiliatedWithDepartment();

} // Department
