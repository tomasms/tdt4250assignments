/**
 */
package tdt4250assignment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person In Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250assignment.PersonInCourse#getPerson <em>Person</em>}</li>
 *   <li>{@link tdt4250assignment.PersonInCourse#getRole <em>Role</em>}</li>
 * </ul>
 *
 * @see tdt4250assignment.Tdt4250assignmentPackage#getPersonInCourse()
 * @model
 * @generated
 */
public interface PersonInCourse extends EObject {
	/**
	 * Returns the value of the '<em><b>Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' reference.
	 * @see #setPerson(Person)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getPersonInCourse_Person()
	 * @model required="true"
	 * @generated
	 */
	Person getPerson();

	/**
	 * Sets the value of the '{@link tdt4250assignment.PersonInCourse#getPerson <em>Person</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person</em>' reference.
	 * @see #getPerson()
	 * @generated
	 */
	void setPerson(Person value);

	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250assignment.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see tdt4250assignment.Role
	 * @see #setRole(Role)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getPersonInCourse_Role()
	 * @model required="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link tdt4250assignment.PersonInCourse#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see tdt4250assignment.Role
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

} // PersonInCourse
