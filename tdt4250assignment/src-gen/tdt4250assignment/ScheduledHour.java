/**
 */
package tdt4250assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduled Hour</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250assignment.ScheduledHour#getDuration <em>Duration</em>}</li>
 *   <li>{@link tdt4250assignment.ScheduledHour#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250assignment.ScheduledHour#getBeginning <em>Beginning</em>}</li>
 *   <li>{@link tdt4250assignment.ScheduledHour#getReservedForProgram <em>Reserved For Program</em>}</li>
 *   <li>{@link tdt4250assignment.ScheduledHour#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250assignment.ScheduledHour#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour()
 * @model
 * @generated
 */
public interface ScheduledHour extends EObject {
	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_Duration()
	 * @model
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link tdt4250assignment.ScheduledHour#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute.
	 * @see #setRoom(String)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_Room()
	 * @model
	 * @generated
	 */
	String getRoom();

	/**
	 * Sets the value of the '{@link tdt4250assignment.ScheduledHour#getRoom <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' attribute.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(String value);

	/**
	 * Returns the value of the '<em><b>Beginning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Beginning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Beginning</em>' attribute.
	 * @see #setBeginning(String)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_Beginning()
	 * @model
	 * @generated
	 */
	String getBeginning();

	/**
	 * Sets the value of the '{@link tdt4250assignment.ScheduledHour#getBeginning <em>Beginning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Beginning</em>' attribute.
	 * @see #getBeginning()
	 * @generated
	 */
	void setBeginning(String value);

	/**
	 * Returns the value of the '<em><b>Reserved For Program</b></em>' reference list.
	 * The list contents are of type {@link tdt4250assignment.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved For Program</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved For Program</em>' reference list.
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_ReservedForProgram()
	 * @model
	 * @generated
	 */
	EList<StudyProgram> getReservedForProgram();

	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250assignment.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see tdt4250assignment.Day
	 * @see #setDay(Day)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_Day()
	 * @model
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link tdt4250assignment.ScheduledHour#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see tdt4250assignment.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250assignment.HourType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see tdt4250assignment.HourType
	 * @see #setType(HourType)
	 * @see tdt4250assignment.Tdt4250assignmentPackage#getScheduledHour_Type()
	 * @model
	 * @generated
	 */
	HourType getType();

	/**
	 * Sets the value of the '{@link tdt4250assignment.ScheduledHour#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see tdt4250assignment.HourType
	 * @see #getType()
	 * @generated
	 */
	void setType(HourType value);

} // ScheduledHour
