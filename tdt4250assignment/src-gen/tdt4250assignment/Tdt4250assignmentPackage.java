/**
 */
package tdt4250assignment;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250assignment.Tdt4250assignmentFactory
 * @model kind="package"
 * @generated
 */
public interface Tdt4250assignmentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tdt4250assignment";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/tdt4250assignment";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tdt4250assignment";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Tdt4250assignmentPackage eINSTANCE = tdt4250assignment.impl.Tdt4250assignmentPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.CourseImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 1;

	/**
	 * The feature id for the '<em><b>Credits Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS_NUMBER = 2;

	/**
	 * The feature id for the '<em><b>Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__INSTANCES = 3;

	/**
	 * The feature id for the '<em><b>Required Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIRED_COURSES = 4;

	/**
	 * The feature id for the '<em><b>Recommended Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RECOMMENDED_COURSES = 5;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 6;

	/**
	 * The feature id for the '<em><b>Credits Reduction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS_REDUCTION = 7;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DEPARTMENT = 8;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.CourseRelationImpl <em>Course Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.CourseRelationImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseRelation()
	 * @generated
	 */
	int COURSE_RELATION = 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Credits Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION__CREDITS_REDUCTION = 1;

	/**
	 * The number of structural features of the '<em>Course Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.CourseInstanceImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Evaluation Form</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EVALUATION_FORM = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 1;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LAB_HOURS = 2;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LECTURE_HOURS = 3;

	/**
	 * The feature id for the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE_WORK = 4;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STUDY_PROGRAMS = 5;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE = 6;

	/**
	 * The feature id for the '<em><b>Person In Course</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__PERSON_IN_COURSE = 7;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.CourseWorkImpl <em>Course Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.CourseWorkImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseWork()
	 * @generated
	 */
	int COURSE_WORK = 3;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.PersonImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = 0;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.StudyProgramImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 5;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = 0;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.WorkImpl <em>Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.WorkImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getWork()
	 * @generated
	 */
	int WORK = 6;

	/**
	 * The feature id for the '<em><b>Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__PERCENTAGE = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.DepartmentImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 1;

	/**
	 * The feature id for the '<em><b>Person Affiliated With Department</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT = 2;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.ScheduledHourImpl <em>Scheduled Hour</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.ScheduledHourImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getScheduledHour()
	 * @generated
	 */
	int SCHEDULED_HOUR = 8;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__DURATION = 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__ROOM = 1;

	/**
	 * The feature id for the '<em><b>Beginning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__BEGINNING = 2;

	/**
	 * The feature id for the '<em><b>Reserved For Program</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__RESERVED_FOR_PROGRAM = 3;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__DAY = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__TYPE = 5;

	/**
	 * The number of structural features of the '<em>Scheduled Hour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Scheduled Hour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.impl.PersonInCourseImpl <em>Person In Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.impl.PersonInCourseImpl
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getPersonInCourse()
	 * @generated
	 */
	int PERSON_IN_COURSE = 9;

	/**
	 * The feature id for the '<em><b>Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_IN_COURSE__PERSON = 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_IN_COURSE__ROLE = 1;

	/**
	 * The number of structural features of the '<em>Person In Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_IN_COURSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Person In Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_IN_COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250assignment.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.Day
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getDay()
	 * @generated
	 */
	int DAY = 10;

	/**
	 * The meta object id for the '{@link tdt4250assignment.WorkType <em>Work Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.WorkType
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getWorkType()
	 * @generated
	 */
	int WORK_TYPE = 11;

	/**
	 * The meta object id for the '{@link tdt4250assignment.HourType <em>Hour Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.HourType
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getHourType()
	 * @generated
	 */
	int HOUR_TYPE = 12;

	/**
	 * The meta object id for the '{@link tdt4250assignment.Role <em>Role</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250assignment.Role
	 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 13;

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250assignment.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250assignment.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250assignment.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Course#getCreditsNumber <em>Credits Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Number</em>'.
	 * @see tdt4250assignment.Course#getCreditsNumber()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_CreditsNumber();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.Course#getInstances <em>Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instances</em>'.
	 * @see tdt4250assignment.Course#getInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Instances();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250assignment.Course#getRequiredCourses <em>Required Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Courses</em>'.
	 * @see tdt4250assignment.Course#getRequiredCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RequiredCourses();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250assignment.Course#getRecommendedCourses <em>Recommended Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recommended Courses</em>'.
	 * @see tdt4250assignment.Course#getRecommendedCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RecommendedCourses();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see tdt4250assignment.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.Course#getCreditsReduction <em>Credits Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Credits Reduction</em>'.
	 * @see tdt4250assignment.Course#getCreditsReduction()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CreditsReduction();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250assignment.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see tdt4250assignment.Course#getDepartment()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Department();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.CourseRelation <em>Course Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Relation</em>'.
	 * @see tdt4250assignment.CourseRelation
	 * @generated
	 */
	EClass getCourseRelation();

	/**
	 * Returns the meta object for the reference '{@link tdt4250assignment.CourseRelation#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course</em>'.
	 * @see tdt4250assignment.CourseRelation#getCourse()
	 * @see #getCourseRelation()
	 * @generated
	 */
	EReference getCourseRelation_Course();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.CourseRelation#getCreditsReduction <em>Credits Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Reduction</em>'.
	 * @see tdt4250assignment.CourseRelation#getCreditsReduction()
	 * @see #getCourseRelation()
	 * @generated
	 */
	EAttribute getCourseRelation_CreditsReduction();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see tdt4250assignment.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.CourseInstance#getEvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evaluation Form</em>'.
	 * @see tdt4250assignment.CourseInstance#getEvaluationForm()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_EvaluationForm();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see tdt4250assignment.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.CourseInstance#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lab Hours</em>'.
	 * @see tdt4250assignment.CourseInstance#getLabHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_LabHours();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.CourseInstance#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lecture Hours</em>'.
	 * @see tdt4250assignment.CourseInstance#getLectureHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_LectureHours();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250assignment.CourseInstance#getCourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Course Work</em>'.
	 * @see tdt4250assignment.CourseInstance#getCourseWork()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_CourseWork();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250assignment.CourseInstance#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Study Programs</em>'.
	 * @see tdt4250assignment.CourseInstance#getStudyPrograms()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_StudyPrograms();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250assignment.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250assignment.CourseInstance#getCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Course();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.CourseInstance#getPersonInCourse <em>Person In Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Person In Course</em>'.
	 * @see tdt4250assignment.CourseInstance#getPersonInCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_PersonInCourse();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.CourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Work</em>'.
	 * @see tdt4250assignment.CourseWork
	 * @generated
	 */
	EClass getCourseWork();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.CourseWork#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see tdt4250assignment.CourseWork#getLectureHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.CourseWork#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see tdt4250assignment.CourseWork#getLabHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LabHours();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see tdt4250assignment.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Person#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250assignment.Person#getName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see tdt4250assignment.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.StudyProgram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250assignment.StudyProgram#getCode()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Code();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.Work <em>Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Work</em>'.
	 * @see tdt4250assignment.Work
	 * @generated
	 */
	EClass getWork();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Work#getPercentage <em>Percentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Percentage</em>'.
	 * @see tdt4250assignment.Work#getPercentage()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Percentage();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Work#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tdt4250assignment.Work#getType()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Type();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see tdt4250assignment.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250assignment.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see tdt4250assignment.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250assignment.Department#getPersonAffiliatedWithDepartment <em>Person Affiliated With Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Person Affiliated With Department</em>'.
	 * @see tdt4250assignment.Department#getPersonAffiliatedWithDepartment()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_PersonAffiliatedWithDepartment();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.ScheduledHour <em>Scheduled Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduled Hour</em>'.
	 * @see tdt4250assignment.ScheduledHour
	 * @generated
	 */
	EClass getScheduledHour();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.ScheduledHour#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see tdt4250assignment.ScheduledHour#getDuration()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Duration();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.ScheduledHour#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see tdt4250assignment.ScheduledHour#getRoom()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Room();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.ScheduledHour#getBeginning <em>Beginning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Beginning</em>'.
	 * @see tdt4250assignment.ScheduledHour#getBeginning()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Beginning();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250assignment.ScheduledHour#getReservedForProgram <em>Reserved For Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reserved For Program</em>'.
	 * @see tdt4250assignment.ScheduledHour#getReservedForProgram()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EReference getScheduledHour_ReservedForProgram();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.ScheduledHour#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see tdt4250assignment.ScheduledHour#getDay()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Day();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.ScheduledHour#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tdt4250assignment.ScheduledHour#getType()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Type();

	/**
	 * Returns the meta object for class '{@link tdt4250assignment.PersonInCourse <em>Person In Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person In Course</em>'.
	 * @see tdt4250assignment.PersonInCourse
	 * @generated
	 */
	EClass getPersonInCourse();

	/**
	 * Returns the meta object for the reference '{@link tdt4250assignment.PersonInCourse#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Person</em>'.
	 * @see tdt4250assignment.PersonInCourse#getPerson()
	 * @see #getPersonInCourse()
	 * @generated
	 */
	EReference getPersonInCourse_Person();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250assignment.PersonInCourse#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see tdt4250assignment.PersonInCourse#getRole()
	 * @see #getPersonInCourse()
	 * @generated
	 */
	EAttribute getPersonInCourse_Role();

	/**
	 * Returns the meta object for enum '{@link tdt4250assignment.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see tdt4250assignment.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link tdt4250assignment.WorkType <em>Work Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Work Type</em>'.
	 * @see tdt4250assignment.WorkType
	 * @generated
	 */
	EEnum getWorkType();

	/**
	 * Returns the meta object for enum '{@link tdt4250assignment.HourType <em>Hour Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Hour Type</em>'.
	 * @see tdt4250assignment.HourType
	 * @generated
	 */
	EEnum getHourType();

	/**
	 * Returns the meta object for enum '{@link tdt4250assignment.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Role</em>'.
	 * @see tdt4250assignment.Role
	 * @generated
	 */
	EEnum getRole();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Tdt4250assignmentFactory getTdt4250assignmentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.CourseImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Credits Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS_NUMBER = eINSTANCE.getCourse_CreditsNumber();

		/**
		 * The meta object literal for the '<em><b>Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__INSTANCES = eINSTANCE.getCourse_Instances();

		/**
		 * The meta object literal for the '<em><b>Required Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIRED_COURSES = eINSTANCE.getCourse_RequiredCourses();

		/**
		 * The meta object literal for the '<em><b>Recommended Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RECOMMENDED_COURSES = eINSTANCE.getCourse_RecommendedCourses();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits Reduction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__CREDITS_REDUCTION = eINSTANCE.getCourse_CreditsReduction();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__DEPARTMENT = eINSTANCE.getCourse_Department();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.CourseRelationImpl <em>Course Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.CourseRelationImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseRelation()
		 * @generated
		 */
		EClass COURSE_RELATION = eINSTANCE.getCourseRelation();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_RELATION__COURSE = eINSTANCE.getCourseRelation_Course();

		/**
		 * The meta object literal for the '<em><b>Credits Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_RELATION__CREDITS_REDUCTION = eINSTANCE.getCourseRelation_CreditsReduction();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.CourseInstanceImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Evaluation Form</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EVALUATION_FORM = eINSTANCE.getCourseInstance_EvaluationForm();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__LAB_HOURS = eINSTANCE.getCourseInstance_LabHours();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__LECTURE_HOURS = eINSTANCE.getCourseInstance_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Course Work</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE_WORK = eINSTANCE.getCourseInstance_CourseWork();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__STUDY_PROGRAMS = eINSTANCE.getCourseInstance_StudyPrograms();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE = eINSTANCE.getCourseInstance_Course();

		/**
		 * The meta object literal for the '<em><b>Person In Course</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__PERSON_IN_COURSE = eINSTANCE.getCourseInstance_PersonInCourse();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.CourseWorkImpl <em>Course Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.CourseWorkImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getCourseWork()
		 * @generated
		 */
		EClass COURSE_WORK = eINSTANCE.getCourseWork();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LECTURE_HOURS = eINSTANCE.getCourseWork_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LAB_HOURS = eINSTANCE.getCourseWork_LabHours();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.PersonImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__NAME = eINSTANCE.getPerson_Name();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.StudyProgramImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__CODE = eINSTANCE.getStudyProgram_Code();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.WorkImpl <em>Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.WorkImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getWork()
		 * @generated
		 */
		EClass WORK = eINSTANCE.getWork();

		/**
		 * The meta object literal for the '<em><b>Percentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__PERCENTAGE = eINSTANCE.getWork_Percentage();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__TYPE = eINSTANCE.getWork_Type();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.DepartmentImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Person Affiliated With Department</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT = eINSTANCE
				.getDepartment_PersonAffiliatedWithDepartment();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.ScheduledHourImpl <em>Scheduled Hour</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.ScheduledHourImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getScheduledHour()
		 * @generated
		 */
		EClass SCHEDULED_HOUR = eINSTANCE.getScheduledHour();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__DURATION = eINSTANCE.getScheduledHour_Duration();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__ROOM = eINSTANCE.getScheduledHour_Room();

		/**
		 * The meta object literal for the '<em><b>Beginning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__BEGINNING = eINSTANCE.getScheduledHour_Beginning();

		/**
		 * The meta object literal for the '<em><b>Reserved For Program</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULED_HOUR__RESERVED_FOR_PROGRAM = eINSTANCE.getScheduledHour_ReservedForProgram();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__DAY = eINSTANCE.getScheduledHour_Day();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__TYPE = eINSTANCE.getScheduledHour_Type();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.impl.PersonInCourseImpl <em>Person In Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.impl.PersonInCourseImpl
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getPersonInCourse()
		 * @generated
		 */
		EClass PERSON_IN_COURSE = eINSTANCE.getPersonInCourse();

		/**
		 * The meta object literal for the '<em><b>Person</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_IN_COURSE__PERSON = eINSTANCE.getPersonInCourse_Person();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_IN_COURSE__ROLE = eINSTANCE.getPersonInCourse_Role();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.Day
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.WorkType <em>Work Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.WorkType
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getWorkType()
		 * @generated
		 */
		EEnum WORK_TYPE = eINSTANCE.getWorkType();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.HourType <em>Hour Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.HourType
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getHourType()
		 * @generated
		 */
		EEnum HOUR_TYPE = eINSTANCE.getHourType();

		/**
		 * The meta object literal for the '{@link tdt4250assignment.Role <em>Role</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250assignment.Role
		 * @see tdt4250assignment.impl.Tdt4250assignmentPackageImpl#getRole()
		 * @generated
		 */
		EEnum ROLE = eINSTANCE.getRole();

	}

} //Tdt4250assignmentPackage
