package tdt4250assignment.genHtml

import tdt4250assignment.Course
import java.io.IOException
import java.util.Arrays
import org.eclipse.emf.common.util.URI
import java.io.PrintStream
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.EObject
import tdt4250assignment.Tdt4250assignmentPackage
import tdt4250assignment.util.Tdt4250assignmentResourceFactoryImpl
import tdt4250assignment.Department
import tdt4250assignment.CourseInstance
import tdt4250assignment.Work
import tdt4250assignment.ScheduledHour
import tdt4250assignment.PersonInCourse


class GenHtmlFromCourseModel {

	def String generateHtml(Course course) {
		generateHtml(course, new StringBuilder).toString
	}

	def CharSequence generateHtml(Course course, StringBuilder stringBuilder) {
		generatePreHtml(course.code, course.name, stringBuilder)
		generateContent(course, stringBuilder);
		generatePostHtml(stringBuilder)
	}
	

	def CharSequence generatePreHtml(String code, String name, StringBuilder stringBuilder) {
		stringBuilder << '''
<!DOCTYPE html>
<html>
<head>
	<title>�code� - �name�"</title>
	<meta charset="utf-8"/>
	<style>
	table, th, td {
	    border: 1px solid black;
	}
	</style>	
</head>
<body>
'''
	}
	
	def operator_lessThan(StringBuilder builder, String string) {
		
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	def CharSequence generatePostHtml(StringBuilder stringBuilder) {
		stringBuilder.append("\n</body></html>")
	}
	
	def CharSequence generateContent(Course course, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h1>�course.code� - �course.name�</h1>
			<h2>Content</h2>
			�course.content�
		'''
		for (CourseInstance ci : course.instances) {
			stringBuilder << '''
				<h2>�ci.year�</h2>  
			'''
			generateEvaluationForm(ci, stringBuilder);
			generateTimetable(ci, stringBuilder);
			generatePersonInCourse(ci, stringBuilder);
		}
		//generateRecommendedCourses(course, stringBuilder);
		stringBuilder << ''''''
	}

	def CharSequence generateTimetable(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h3>Timetable</h3>
			<table>
			       <tr>
			           <th>Day</th>
			           <th>Type</th>
			           <th>Room</th>
			           <th>Time</th>
			           <th>Duration</th>
			       </tr>
		'''
		for (ScheduledHour lh : courseInstance.labHours) {
			stringBuilder << '''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			'''
		}
		for (ScheduledHour lh : courseInstance.lectureHours) {
			stringBuilder << '''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	
	}
	
	def CharSequence generateEvaluationForm(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h3>Examination arrangement</h3>
			<table>
			       <tr>
			           <th>Evaluation Form</th>
			           <th>Weighting</th>
			       </tr>
		'''
		for (Work w : courseInstance.evaluationForm) {
			stringBuilder << '''
				<tr>
					<td>�w.type�</td>
					<td>�w.percentage�/100</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	}
	
		def CharSequence generatePersonInCourse(CourseInstance courseInstance, StringBuilder stringBuilder) {
				stringBuilder << '''
			<h3>Contact information</h3>
			Department with academic responsibility : 
			�courseInstance.course.department.name�
			<table>
			       <tr>
			           <th>Name</th>
			           <th>Role</th>
			       </tr>
		'''
		for (PersonInCourse p : courseInstance.personInCourse) {
			stringBuilder << '''
				<tr>
					<td>�p.person.name�</td>
					<td>�p.role�</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	}
	
		def StringBuilder operator_doubleLessThan(StringBuilder stringBuilder, Object o) {
		return stringBuilder.append(o);
	}
	
	def static void main(String[] args) throws IOException {
		val argsAsList = Arrays.asList(args)
		val course = if(argsAsList.size > 0) getCourse(argsAsList.get(0)) else getSampleCourse();
		val html = new GenHtmlFromCourseModel().generateHtml(course);
		if (args.length > 1) {
			val target = URI.createURI(argsAsList.get(1));
			val ps = new PrintStream(course.eResource().getResourceSet().getURIConverter().createOutputStream(target))
			ps.print(html);
		} else {
			System.out.println(html);
		}
	}
	
	def static Course getSampleCourse() {
		try {
			return getCourse(GenHtmlFromCourseModel.getResource("TDT4250.xmi").toString());
		} catch (IOException e) {
			System.err.println(e);
			return null;
		}
	}
	
	def static Course getCourse(String uriString) throws IOException {
		val resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(Tdt4250assignmentPackage.eNS_URI, Tdt4250assignmentPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new Tdt4250assignmentResourceFactoryImpl());
		val resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Department) {
				return eObject.courses.get(0);
			}
		}
		return null;
	}

}
