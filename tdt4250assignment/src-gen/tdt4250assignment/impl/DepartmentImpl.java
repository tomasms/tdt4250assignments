/**
 */
package tdt4250assignment.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250assignment.Course;
import tdt4250assignment.Department;
import tdt4250assignment.Person;
import tdt4250assignment.Tdt4250assignmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250assignment.impl.DepartmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250assignment.impl.DepartmentImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250assignment.impl.DepartmentImpl#getPersonAffiliatedWithDepartment <em>Person Affiliated With Department</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getPersonAffiliatedWithDepartment() <em>Person Affiliated With Department</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonAffiliatedWithDepartment()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> personAffiliatedWithDepartment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Tdt4250assignmentPackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Tdt4250assignmentPackage.DEPARTMENT__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentWithInverseEList<Course>(Course.class, this,
					Tdt4250assignmentPackage.DEPARTMENT__COURSES, Tdt4250assignmentPackage.COURSE__DEPARTMENT);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersonAffiliatedWithDepartment() {
		if (personAffiliatedWithDepartment == null) {
			personAffiliatedWithDepartment = new EObjectContainmentEList<Person>(Person.class, this,
					Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT);
		}
		return personAffiliatedWithDepartment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCourses()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			return ((InternalEList<?>) getCourses()).basicRemove(otherEnd, msgs);
		case Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT:
			return ((InternalEList<?>) getPersonAffiliatedWithDepartment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__NAME:
			return getName();
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			return getCourses();
		case Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT:
			return getPersonAffiliatedWithDepartment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__NAME:
			setName((String) newValue);
			return;
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			getCourses().clear();
			getCourses().addAll((Collection<? extends Course>) newValue);
			return;
		case Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT:
			getPersonAffiliatedWithDepartment().clear();
			getPersonAffiliatedWithDepartment().addAll((Collection<? extends Person>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			getCourses().clear();
			return;
		case Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT:
			getPersonAffiliatedWithDepartment().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Tdt4250assignmentPackage.DEPARTMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case Tdt4250assignmentPackage.DEPARTMENT__COURSES:
			return courses != null && !courses.isEmpty();
		case Tdt4250assignmentPackage.DEPARTMENT__PERSON_AFFILIATED_WITH_DEPARTMENT:
			return personAffiliatedWithDepartment != null && !personAffiliatedWithDepartment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl
