/**
 */
package tdt4250assignment.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see tdt4250assignment.util.Tdt4250assignmentResourceFactoryImpl
 * @generated
 */
public class Tdt4250assignmentResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Tdt4250assignmentResourceImpl(URI uri) {
		super(uri);
	}

} //Tdt4250assignmentResourceImpl
