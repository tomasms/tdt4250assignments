package tdt4250assignment.genHtml;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import tdt4250assignment.Course;
import tdt4250assignment.CourseInstance;
import tdt4250assignment.Day;
import tdt4250assignment.Department;
import tdt4250assignment.HourType;
import tdt4250assignment.PersonInCourse;
import tdt4250assignment.Role;
import tdt4250assignment.ScheduledHour;
import tdt4250assignment.Tdt4250assignmentPackage;
import tdt4250assignment.Work;
import tdt4250assignment.WorkType;
import tdt4250assignment.util.Tdt4250assignmentResourceFactoryImpl;

@SuppressWarnings("all")
public class GenHtmlFromCourseModel {
  public String generateHtml(final Course course) {
    StringBuilder _stringBuilder = new StringBuilder();
    return this.generateHtml(course, _stringBuilder).toString();
  }
  
  public CharSequence generateHtml(final Course course, final StringBuilder stringBuilder) {
    CharSequence _xblockexpression = null;
    {
      this.generatePreHtml(course.getCode(), course.getName(), stringBuilder);
      this.generateContent(course, stringBuilder);
      _xblockexpression = this.generatePostHtml(stringBuilder);
    }
    return _xblockexpression;
  }
  
  public CharSequence generatePreHtml(final String code, final String name, final StringBuilder stringBuilder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<!DOCTYPE html>");
    _builder.newLine();
    _builder.append("<html>");
    _builder.newLine();
    _builder.append("<head>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<title>");
    _builder.append(code, "\t");
    _builder.append(" - ");
    _builder.append(name, "\t");
    _builder.append("\"</title>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<meta charset=\"utf-8\"/>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<style>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("table, th, td {");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("border: 1px solid black;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</style>\t");
    _builder.newLine();
    _builder.append("</head>");
    _builder.newLine();
    _builder.append("<body>");
    _builder.newLine();
    return this.operator_doubleLessThan(stringBuilder, _builder);
  }
  
  public void operator_lessThan(final StringBuilder builder, final String string) {
    throw new UnsupportedOperationException("TODO: auto-generated method stub");
  }
  
  public CharSequence generatePostHtml(final StringBuilder stringBuilder) {
    return stringBuilder.append("\n</body></html>");
  }
  
  public CharSequence generateContent(final Course course, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h1>");
      String _code = course.getCode();
      _builder.append(_code);
      _builder.append(" - ");
      String _name = course.getName();
      _builder.append(_name);
      _builder.append("</h1>");
      _builder.newLineIfNotEmpty();
      _builder.append("<h2>Content</h2>");
      _builder.newLine();
      String _content = course.getContent();
      _builder.append(_content);
      _builder.newLineIfNotEmpty();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<CourseInstance> _instances = course.getInstances();
      for (final CourseInstance ci : _instances) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("<h2>");
          int _year = ci.getYear();
          _builder_1.append(_year);
          _builder_1.append("</h2>  ");
          _builder_1.newLineIfNotEmpty();
          this.operator_doubleLessThan(stringBuilder, _builder_1);
          this.generateEvaluationForm(ci, stringBuilder);
          this.generateTimetable(ci, stringBuilder);
          this.generatePersonInCourse(ci, stringBuilder);
        }
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_1);
    }
    return _xblockexpression;
  }
  
  public CharSequence generateTimetable(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Timetable</h3>");
      _builder.newLine();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Day</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Type</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Room</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Time</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Duration</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<ScheduledHour> _labHours = courseInstance.getLabHours();
      for (final ScheduledHour lh : _labHours) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        Day _day = lh.getDay();
        _builder_1.append(_day, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        HourType _type = lh.getType();
        _builder_1.append(_type, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _room = lh.getRoom();
        _builder_1.append(_room, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _beginning = lh.getBeginning();
        _builder_1.append(_beginning, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        int _duration = lh.getDuration();
        _builder_1.append(_duration, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      EList<ScheduledHour> _lectureHours = courseInstance.getLectureHours();
      for (final ScheduledHour lh_1 : _lectureHours) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("<tr>");
        _builder_2.newLine();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        Day _day_1 = lh_1.getDay();
        _builder_2.append(_day_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        HourType _type_1 = lh_1.getType();
        _builder_2.append(_type_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        String _room_1 = lh_1.getRoom();
        _builder_2.append(_room_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        String _beginning_1 = lh_1.getBeginning();
        _builder_2.append(_beginning_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        int _duration_1 = lh_1.getDuration();
        _builder_2.append(_duration_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("</tr>");
        _builder_2.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_3);
    }
    return _xblockexpression;
  }
  
  public CharSequence generateEvaluationForm(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Examination arrangement</h3>");
      _builder.newLine();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Evaluation Form</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Weighting</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<Work> _evaluationForm = courseInstance.getEvaluationForm();
      for (final Work w : _evaluationForm) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        WorkType _type = w.getType();
        _builder_1.append(_type, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        int _percentage = w.getPercentage();
        _builder_1.append(_percentage, "\t");
        _builder_1.append("/100</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_2);
    }
    return _xblockexpression;
  }
  
  public CharSequence generatePersonInCourse(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Contact information</h3>");
      _builder.newLine();
      _builder.append("Department with academic responsibility : ");
      _builder.newLine();
      String _name = courseInstance.getCourse().getDepartment().getName();
      _builder.append(_name);
      _builder.newLineIfNotEmpty();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Name</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Role</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<PersonInCourse> _personInCourse = courseInstance.getPersonInCourse();
      for (final PersonInCourse p : _personInCourse) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _name_1 = p.getPerson().getName();
        _builder_1.append(_name_1, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        Role _role = p.getRole();
        _builder_1.append(_role, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_2);
    }
    return _xblockexpression;
  }
  
  public StringBuilder operator_doubleLessThan(final StringBuilder stringBuilder, final Object o) {
    return stringBuilder.append(o);
  }
  
  public static void main(final String[] args) throws IOException {
    final List<String> argsAsList = Arrays.<String>asList(args);
    Course _xifexpression = null;
    int _size = argsAsList.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      _xifexpression = GenHtmlFromCourseModel.getCourse(argsAsList.get(0));
    } else {
      _xifexpression = GenHtmlFromCourseModel.getSampleCourse();
    }
    final Course course = _xifexpression;
    final String html = new GenHtmlFromCourseModel().generateHtml(course);
    int _length = args.length;
    boolean _greaterThan_1 = (_length > 1);
    if (_greaterThan_1) {
      final URI target = URI.createURI(argsAsList.get(1));
      OutputStream _createOutputStream = course.eResource().getResourceSet().getURIConverter().createOutputStream(target);
      final PrintStream ps = new PrintStream(_createOutputStream);
      ps.print(html);
    } else {
      System.out.println(html);
    }
  }
  
  public static Course getSampleCourse() {
    try {
      return GenHtmlFromCourseModel.getCourse(GenHtmlFromCourseModel.class.getResource("TDT4250.xmi").toString());
    } catch (final Throwable _t) {
      if (_t instanceof IOException) {
        final IOException e = (IOException)_t;
        System.err.println(e);
        return null;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public static Course getCourse(final String uriString) throws IOException {
    final ResourceSetImpl resSet = new ResourceSetImpl();
    resSet.getPackageRegistry().put(Tdt4250assignmentPackage.eNS_URI, Tdt4250assignmentPackage.eINSTANCE);
    Map<String, Object> _extensionToFactoryMap = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
    Tdt4250assignmentResourceFactoryImpl _tdt4250assignmentResourceFactoryImpl = new Tdt4250assignmentResourceFactoryImpl();
    _extensionToFactoryMap.put("xmi", _tdt4250assignmentResourceFactoryImpl);
    final Resource resource = resSet.getResource(URI.createURI(uriString), true);
    EList<EObject> _contents = resource.getContents();
    for (final EObject eObject : _contents) {
      if ((eObject instanceof Department)) {
        return ((Department)eObject).getCourses().get(0);
      }
    }
    return null;
  }
}
